import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/socket.service.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:booking_grab_apis/views/login_screen.dart';
import 'package:booking_grab_apis/views/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class WelcomeScreen extends StatefulWidget {
  @override
  _welcomeScreen createState() => _welcomeScreen();

}
class _welcomeScreen extends State<WelcomeScreen> {
  bool isLauching = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Text('welcome!'),
        ),
      )
    );
  }

  initialize() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    if (token == null) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => LoginScreen()
        )
      );

      return;
    }

    new SocketService(token);
    String userID = sharedPreferences.getString('userID');
    String bookingID = sharedPreferences.getString('bookingID_${userID}');
    VehicleBookings booking = await VehicleBookingsService().findOneBooking(
      bookingID: bookingID
    );

    if (booking != null) {
      MyGlobalVariable.currentBooking = booking;
    }


    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => MainScreen(pageIndex: 0)
      )
    );
  }
}