import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/services/users.service.dart';
import 'package:booking_grab_apis/validators/form_validator.dart';
import 'package:booking_grab_apis/views/login_screen.dart';
import 'package:booking_grab_apis/views/main_screen.dart';
import 'package:flutter/material.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreen createState() => new _SignupScreen();
}

class _SignupScreen extends State<SignupScreen> {
  TextEditingController _firstNameController = new TextEditingController();
  TextEditingController _lastNameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _confirmPasswordController = new TextEditingController();
  TextEditingController _phoneNumberController = new TextEditingController();

  String _firstNameError;
  String _emailError;
  String _phoneNumberError;
  String _passwordError;
  String _confirmPasswordError;

  bool checkingForm = false;
  bool isRegistering = false;

  @override
  void initState() {
    _firstNameError = null;
    _emailError = null;
    _phoneNumberError = null;
    _passwordError = null;
    _confirmPasswordError = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(20),
            color: myColors.whiteBackground,
            child: Column(
              children: [
                Text(
                  'Signup',
                  style: TextStyle(
                    color:  myColors.title,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Roboto',
                    fontSize: 52,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 30,),
                InkWell(
                  onTap: isRegistering ? null : () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => LoginScreen()
                      )
                    );
                  },
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Already have an account? ',
                          style: TextStyle(
                            fontSize: 20
                          ),
                        ),
                        Text(
                          'Sign in',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: myColors.mainColor,
                            fontSize: 20
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                  ),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    enabled: !isRegistering,
                    textAlign: TextAlign.left,
                    controller: _firstNameController,
                    onChanged: (String value) {
                      String error = FormValidator().checkFirstName(value);
                      checkingForm = true;
                      setState(() {
                        _firstNameError = error;
                      });
                    },
                    decoration: InputDecoration(
                      labelText: 'FirstName',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      errorText: checkingForm ? _firstNameError : null,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: myColors.mainColor, 
                          width: 1
                        ),
                        borderRadius:  BorderRadius.all(Radius.circular(6))
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                  ),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.left,
                    enabled: !isRegistering,
                    controller: _lastNameController,
                    decoration: InputDecoration(
                      labelText: 'LastName',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: myColors.mainColor, 
                          width: 1
                        ),
                        borderRadius:  BorderRadius.all(Radius.circular(6))
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                  ),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.left,
                    enabled: !isRegistering,
                    onChanged: (String value) {
                      String error = FormValidator().checkPhoneNumber(value);

                      print(error);
                      checkingForm = true;
                      setState(() {
                        _phoneNumberError = error;
                      });
                    },
                    controller: _phoneNumberController,
                    decoration: InputDecoration(
                      errorText: checkingForm ? _phoneNumberError : null,
                      labelText: 'Phone Number',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: myColors.mainColor, 
                          width: 1
                        ),
                        borderRadius:  BorderRadius.all(Radius.circular(6))
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                  ),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.left,
                    enabled: !isRegistering,
                    onChanged: (String value) {
                      String error = FormValidator().checkEmail(value);
                      checkingForm = true;
                      setState(() {
                        _emailError = error;
                      });
                    },
                    controller: _emailController,
                    decoration: InputDecoration(
                      errorText: checkingForm ? _emailError : null,
                      labelText: 'Email',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: myColors.mainColor, 
                          width: 1
                        ),
                        borderRadius:  BorderRadius.all(Radius.circular(6))
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                  ),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.left,
                    enabled: !isRegistering,
                    onChanged: (String value) {
                      String error = FormValidator().checkPassword(value);
                      checkingForm = true;
                      setState(() {
                        _passwordError = error;
                      });
                    },
                    controller: _passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      errorText: checkingForm ? _passwordError : null,
                      labelText: 'Password',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: myColors.mainColor, 
                          width: 1
                        ),
                        borderRadius:  BorderRadius.all(Radius.circular(6))
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Container(
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                  ),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.left,
                    enabled: !isRegistering,
                    onChanged: (String value) {
                      String error = FormValidator().checkConfirmPassword(value);
                      checkingForm = true;
                      setState(() {
                        _confirmPasswordError = error;
                      });
                    },
                    controller: _confirmPasswordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      errorText: checkingForm ? _confirmPasswordError : null,
                      labelText: 'Confirm password',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: myColors.mainColor, 
                          width: 1
                        ),
                        borderRadius:  BorderRadius.all(Radius.circular(6))
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30,),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Avartar',
                    style: TextStyle(
                      fontSize: 20,
                      color: myColors.placeholderText
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: myColors.mainColor,
                    ),
                    alignment: Alignment.center,
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    width: 100,
                    height: 100,
                    child: IconButton(
                      alignment: Alignment.center,
                      icon: Icon(
                        Icons.add_a_photo,
                        color: myColors.whiteText,
                        size: 40,
                      ),
                      onPressed: null,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    splashColor: Colors.grey,
                    
                    color: isRegistering ? Colors.grey : myColors.mainColor,
                    child: Container(
                      height: 65,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: isRegistering ? RefreshProgressIndicator(

                        ) : Text('Signup',
                          style: TextStyle(
                            color: myColors.whiteText,
                            fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    onPressed: () {
                      if (!isRegistering) {
                        _onSignUp();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ) 
    );
  }

  _onSignUp() async {
    bool isValidForm = this._validateForm();

    print(isValidForm);

    if (isValidForm) {
      setState(() {
        isRegistering = true;
      });

      dynamic response = await UsersService().signUp(
        firstName: this._firstNameController.text,
        lastName: this._lastNameController.text,
        phoneNumber: this._phoneNumberController.text,
        email: this._emailController.text,
        password: this._passwordController.text,
        confirmPassword: this._confirmPasswordController.text,
        avatar: null
      );

      if (response != null) {
        String token = response['accessToken'];
        // save token
        // use user data for a few feature

        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
              builder: (context) => MainScreen()
          )
        );
      }
    }

    setState(() {
      isRegistering = false;
    });

    return;
  }

  bool _validateForm() {
    bool isValid = true;
    
    setState(() {
      checkingForm = true;
    });

    if (this._firstNameController.text != null && FormValidator().checkFirstName(this._firstNameController.text) != null) {
      setState(() {
        _firstNameError = FormValidator().checkFirstName(this._firstNameController.text);
      });

      isValid = false;
    }

    if (this._phoneNumberController.text != null && FormValidator().validatePhoneNumber(this._phoneNumberController.text) != null) {
      setState(() {
        _phoneNumberError =  FormValidator().validatePhoneNumber(this._phoneNumberController.text);
      });

      isValid = false;
    }

    if (this._emailController.text != null && FormValidator().checkEmail(this._emailController.text) != null) {
      setState(() {
        _emailError = FormValidator().checkEmail(this._emailController.text);
      });

      isValid = false;
    }

    if (this._passwordController.text != null && FormValidator().validatePassword(this._passwordController.text) != null) {
      setState(() {
        _passwordError = FormValidator().validatePassword(this._passwordController.text);
      });

      isValid = false;
    }

    if (this._confirmPasswordController.text != null && FormValidator().validateConfimPassWord(this._passwordController.text, this._confirmPasswordController.text) != null) {
      setState(() {
        _confirmPasswordError = FormValidator().validateConfimPassWord(this._passwordController.text, this._confirmPasswordController.text);
      });

      isValid = false;
    }

    
    return isValid;
  }
}