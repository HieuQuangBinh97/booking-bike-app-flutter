import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/main.dart';
import 'package:booking_grab_apis/models/drivers.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/drivers.service.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:booking_grab_apis/views/driver_journey_screen.dart';
import 'package:booking_grab_apis/views/split_items/dialog_register_driver.dart';
import 'package:flutter/material.dart';

class DriverScreen extends StatefulWidget {
  @override
  _driverScreen createState() => _driverScreen();
}

class _driverScreen extends State<DriverScreen> with SingleTickerProviderStateMixin {
  bool agreeTerm = false;
  bool isFails = false;

  dynamic driverData;

  AnimationController animationControllerOne;
  Animation<Color> animationOne;
  Animation<Color> animationTwo;
  List<VehicleBookings> listNearlyBookings;
  List<Widget> listNearlyBookingsWidget;
  DriversService driversService;

  @override
  void initState() {
    driversService = new DriversService();
    _loadDriver();
    super.initState();

    animationControllerOne = AnimationController(
        duration: Duration(milliseconds: 2000),
        vsync: this
    );
    animationOne = ColorTween(begin: Colors.grey,end: Colors.white70).animate(animationControllerOne);
    animationTwo = ColorTween(begin: Colors.white70,end: Colors.grey).animate(animationControllerOne);
    animationControllerOne.forward();
    animationControllerOne.addListener((){
      if(animationControllerOne.status == AnimationStatus.completed){
        animationControllerOne.reverse();
      } else if(animationControllerOne.status == AnimationStatus.dismissed){
        animationControllerOne.forward();
      }
      this.setState((){});
    });
  }

  @override
  void dispose() {
    if (animationControllerOne != null) {
      animationControllerOne.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        child: driverData == null ? _buildWaitingShaderMask() : ListView(
          children: [
            Row(
              children: [
                Text(
                  'Tài xế',
                  style: TextStyle(
                    color:  myColors.title,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Roboto',
                    fontSize: 40,
                  ),
                )
              ],
            ),
            SizedBox(height: 10,),
            Text(
              'Thống kê',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold
              )
            ),
            SizedBox(height: 20,),
            Container(
              child: Row(
                children: [
                  Container(
                    width: (MediaQuery.of(context).size.width - 40 ) / 2 ,
                    height: 100,
                    decoration: BoxDecoration(
                      color: myColors.whiteBackground,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 20,
                          color: Colors.grey,
                          spreadRadius: 1
                        )
                      ]
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Tổng số chuyến', style: TextStyle(fontSize: 20),),
                        Text('${driverData['vehicleBookings']['totalCount']}', style: TextStyle(fontSize: 20),)
                      ],
                    ),
                  ),
                  SizedBox(width: 20,),
                  Container(
                    height: 100,
                    width: (MediaQuery.of(context).size.width - 40 ) / 2 ,
                    decoration: BoxDecoration(
                      color: myColors.whiteBackground,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 20,
                          color: Colors.grey,
                          spreadRadius: 1
                        )
                      ]
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Thu nhập (VND)', style: TextStyle(fontSize: 20),),
                        Text('1.000.000', style: TextStyle(fontSize: 20),)
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
              decoration: BoxDecoration(
                color: myColors.whiteBackground,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 20,
                    color: Colors.grey,
                    spreadRadius: 1
                  )
                ]
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Text('Tổng số đánh giá:', style: TextStyle(fontSize: 20),),
                      SizedBox(width: 10,),
                      Text('${driverData['driver']['rateTimes']}', style: TextStyle(fontSize: 20),)
                    ],
                  ),
                  Row(
                    children: [
                      Text('Đánh giá:',
                        style: TextStyle(
                          fontSize: 20
                        ),
                      ),
                      Icon(
                        driverData['driver']['rating'] >= 1 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                        size: 40,
                      ),
                      Icon(
                        driverData['driver']['rating'] >= 2 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                        size: 40,
                      ),
                      Icon(
                        driverData['driver']['rating'] >= 3 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                        size: 40,
                      ),
                      Icon(
                        driverData['driver']['rating'] >= 4 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                        size: 40,
                      ),
                      Icon(
                        driverData['driver']['rating'] >= 5 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                        size: 40,
                      )
                    ]
                  )
                ],
              ),
            ),
            SizedBox(height: 30,),
            Text(
              'Chuyến gần đây',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold
              )
            ),
            SizedBox(height: 10,),
            Column(
              children: listNearlyBookingsWidget,
            )
          ],
        ),
      )
    );
  }

  buildListHistory() async {
    try {
      listNearlyBookings = new List<VehicleBookings>();

      dynamic resultBooking = this.driverData['vehicleBookings'];

      if (resultBooking == null || resultBooking['list'] == null || resultBooking['list'].length == null || resultBooking['list'].length == 0 ) {
        setState(() {
            this.listNearlyBookingsWidget = [
              Container(
                height: 100,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.grey[200],
                ),
                child: Center( child: Text('chưa có chuyến đi nào gần đây')),
              )
            ];
          });
          return;
      }

      resultBooking['list'].forEach((json) {
        this.listNearlyBookings.add(new VehicleBookings.fromJson(json));
      });

      List<Widget> widget = new List<Widget>();

      if (listNearlyBookings.length == 0) {
        setState(() {
          this.listNearlyBookingsWidget = [
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Colors.grey[200],
              ),
              child: Center( child: Text('chưa có chuyến đi nào gần đây')),
            )
          ];
        });
        return;
      }
      
      listNearlyBookings.forEach((booking) { 
        dynamic status = getStatus(booking.status);
        Widget action = (booking.status != null && (booking.status == 'PROCESSING' ||
        booking.status == 'DRIVER_ACCEPTED' ||
        booking.status == 'DRIVER_IS_COMING')) ? RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: Text(
              'Xem hành trình',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20
              ),
            ),
          ),
          color: myColors.mainColor,
          onPressed: () async {
            Navigator.of(context).push(
                MaterialPageRoute(
                builder: (context) => DriverJourneyScreen(booking: booking,)
              )
            );
          },
        ) : Container(
          height: 40,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Flexible(
                  child: Container(
                    width: 150,
                    height: 30,
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: status['statusColor']
                    ),
                    child: Center(child: Text(status['status'] != null ? status['status'] : 'không xác định', style: TextStyle(color: myColors.whiteText),)),
                  )
                )
              ],
            ),
          )
        );

        widget.add(
          new Container(
            height: 200,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: myColors.whiteBackground,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  blurRadius: 20,
                  color: Colors.grey,
                  spreadRadius: 1
                )
              ]
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.start,
                //   mainAxisSize: MainAxisSize.max,
                //   children: [
                //     Flexible(
                //       child: Container(
                //         width: 150,
                //         height: 30,
                //         padding: EdgeInsets.all(3),
                //         decoration: BoxDecoration(
                //           borderRadius: BorderRadius.circular(5),
                //           color: status['statusColor']
                //         ),
                //         child: Center(child: Text(status['status'], style: TextStyle(color: myColors.whiteText),)),
                //       )
                //     )
                //   ],
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Icon(
                      Icons.location_pin
                    ),
                    SizedBox(width: 10,),
                    Flexible(
                      child: Text(
                        booking.startAddress,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: myColors.placeholderText,
                          fontSize: 18,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Icon(
                      Icons.outlined_flag
                    ),
                    SizedBox(width: 10,),
                    Flexible(
                      child: Text(
                        booking.destinationAddress,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: myColors.placeholderText,
                          fontSize: 18,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '${((booking.distances).round() / 667).round()} Phút (${((booking.distances).round() / 1000) } km)',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: myColors.mainColor
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Giá:',
                      style: TextStyle(
                        fontSize: 18,
                        color: myColors.placeholderText
                      ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      _getPriceAsString(booking.price ),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: myColors.accentColor,
                        fontSize: 20
                      ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      '(VND)',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: myColors.placeholderText,
                        fontSize: 25
                      ),
                    )
                  ],
                ),
                action,
              ],
            ),
          ),
        );
      });

      setState(() {
        this.listNearlyBookingsWidget = widget;
      });
    } catch(error) {
      setState(() {
        this.listNearlyBookingsWidget = [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: Colors.grey[200],
            ),
            child: Text('Lỗi khi tải dữ liệu'),
          )
        ];
      });
      return;
    }
  }

  dynamic getStatus(String status) {
    Color statusColors = Colors.blueAccent;
    String statusAsString = '';
    switch(status) {
      case 'PENDING':
        statusAsString = 'Chưa chọn tài xế';
        break;
      case 'PROCESSING':
        statusAsString = 'Đang kết nối đến tài xế';
        statusColors = Colors.yellowAccent;
        break;
      case 'DRIVER_ACCEPTED':
        statusAsString = 'Tài xế đã chấp nhận';
        statusColors = Colors.greenAccent;
        break;
      case 'DRIVER_DENIED':
        statusAsString = 'Tài xế đã từ chối';
        statusColors = Colors.grey[600];
        break;
      case 'CANCELED':
        statusAsString = 'Đã hủy';
        statusColors = Colors.redAccent;
        break;
      case 'FINISHED':
        statusAsString = 'Hoàn thành';
        statusColors = Colors.cyanAccent;
        break;
    }

    return {
      'status': statusAsString,
      'statusColor': statusColors
    };
  }

  String _getPriceAsString(double price) {
    int temp = (price).round();
    
    String priceAsString = '';

    while(temp >= 1000) {
      temp = (temp / 1000).toInt();

      priceAsString = priceAsString + '.000';
    }

    priceAsString = '${temp}${priceAsString}';

    return priceAsString;
  }

  Widget _buildWaitingShaderMask() {
    if (isFails) {
      return Container(
        child: Center(
          child: Text('Lỗi khi tải dữ liệu'),
        ),
      );
    }

    return Container(
      child: SafeArea(
        child: ShaderMask(
          shaderCallback: (rect){
            return LinearGradient(
              tileMode: TileMode.clamp,
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [animationOne.value,animationTwo.value]).createShader(rect,textDirection: TextDirection.ltr
            );
          },
          child: SizedBox(
            height: MediaQuery.of(context).size.height-50,
            child: ListView(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        width: (MediaQuery.of(context).size.width - 40 ) / 2 ,
                        height: 100,
                        decoration: BoxDecoration(
                          color: myColors.whiteBackground,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      SizedBox(width: 20,),
                      Container(
                        height: 100,
                        width: (MediaQuery.of(context).size.width - 40 ) / 2 ,
                        decoration: BoxDecoration(
                          color: myColors.whiteBackground,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                SizedBox(height: 30,),
                Container(
                  width: 200,
                  height: 20,
                  color: myColors.whiteBackground,
                ),
                SizedBox(height: 10,),
                Column(
                  children: [
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      color: myColors.whiteBackground,
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      color: myColors.whiteBackground,
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      color: myColors.whiteBackground,
                    ),
                    SizedBox(height: 10,),
                  ],
                )
              ],
            )
          )
        ),
      )
    );
  }

  _loadDriver() async {
    dynamic data = await this.driversService.getDriver();

    if (data == null) {
      setState(() {
        isFails = true;
      });

      this.animationControllerOne.stop();

      return;
    }

    if (data['code'] != null && data['code'] == '404') {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) => DialogRegisterDriver()
        )
      ).then((value) {
        setState(() {
          
        });
      });

      return;
    }

    setState(() {
      this.driverData = data;
    });
    buildListHistory();
  }

}