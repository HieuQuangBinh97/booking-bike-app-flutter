import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/models/position.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/drivers.service.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:booking_grab_apis/views/journey_screen.dart';
import 'package:booking_grab_apis/views/search_place_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookingScreen extends StatefulWidget {
  static LatLng searchLocation;
  static String searchPlace;

  @override
  _bookingScreen createState() => _bookingScreen();

}

class _bookingScreen extends State<BookingScreen> {
  GoogleMapController mapController;
  PolylinePoints polylinePoints;
  Set<Polyline> setPolylines;
  List<LatLng> polylineCoordinates = [];
  Map<PolylineId, Polyline> polylines = {};
  LatLng _currentLocation;
  LatLng _destination;
  final TextEditingController _textController = new TextEditingController();
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  final Set<Marker> _markers = {};

  bool isBooking;
  bool isBooked;
  var modalBottomScheetController;

  VehicleBookings booking;
  dynamic listDriverNearLocation;
  String currentAddress;
  String destinationAddress;
  double distance;
  
  @override
  void initState() {
    isBooking = false;
    isBooked = false;
    _initState();
    this.booking = MyGlobalVariable.currentBooking == null ? null : MyGlobalVariable.currentBooking;
    _textController.text = BookingScreen.searchPlace == null || BookingScreen.searchPlace == "" ? null : BookingScreen.searchPlace;
    destinationAddress = BookingScreen.searchPlace == null || BookingScreen.searchPlace == "" ? null : BookingScreen.searchPlace;
    _getCurrentLocation();
    // _onAddMarker();
    // _createPolylines(_currentLocation, _destination);
    super.initState();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  void _onUpdateCamera() {
    LatLng southwest = _currentLocation;
    LatLng northeast = _destination;
    if ((_currentLocation.latitude - _destination.latitude).abs() < 0.1) {
      return;
    }

    if (_currentLocation.latitude > _destination.latitude) {
      southwest = _destination;
      northeast = _currentLocation;
    }

    mapController.moveCamera(CameraUpdate.newLatLngBounds(LatLngBounds(
      southwest: southwest,
      northeast: northeast
    ), 130));
  }

  @override
  Widget build(BuildContext context) {
    return ProgressDialog(child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Đặt xe',
            style: TextStyle(
              fontSize: 25
            ),
          ),
          backgroundColor: myColors.mainColor,
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left,
              size: 40,
            ),
            onPressed: () {
              MyGlobalVariable.currentBooking = this.booking;
              MyGlobalVariable.listDrivers = this.listDriverNearLocation;
              Navigator.of(context).pop();
            }
          ),
        ),
        body: Container(
          child: Stack(
            children: [
              _currentLocation != null ? GoogleMap(
                polylines: setPolylines,
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: _destination == null ? _currentLocation : _destination,
                  zoom: 15,
                ),
                markers: _markers,
                trafficEnabled: true,
              ) : Center(child: CircularProgressIndicator()),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 105,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: myColors.whiteBackground,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[400],
                              blurRadius: 20,
                              spreadRadius: 4,
                            )
                          ]
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              width: 50,
                              height: 50,
                              child: Icon(
                                Icons.location_on_outlined,
                                color: myColors.mainColor,
                                size: 30,
                              ),
                            ),
                            Container(
                              child: InkWell(
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  width: MediaQuery.of(context).size.width - 120,
                                  height: 60,
                                  decoration: BoxDecoration(
                                    color: myColors.whiteBackground,
                                    border: Border.all(
                                      color: myColors.mainColor,
                                      width: 1.5
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Text(

                                    this.destinationAddress == null ? 'Điểm đến' : this.destinationAddress,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(builder: (context) => CustomSearchScaffold())
                                  ).then((value) {
                                    setState(() {
                                      this.destinationAddress = BookingScreen.searchPlace;
                                      this._destination = BookingScreen.searchLocation;
                                    });
                                  });
                                }
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child:  Container(
                  margin: EdgeInsets.all(20),
                  alignment: Alignment.center,
                  height: 60,
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                    ),
                    child: Center(
                      child: Text(
                        'Tiếp tục',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color:  myColors.mainColor,
                    onPressed: () {
                      _onGetInfomationOfRoute(context);
                    },
                  ),
                ),
              ),
            ],
          ),
        )
      ));
  }
  
  _getCurrentLocation() async {
    try {
      Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
      
      setState(() {
        print(position);
        _currentLocation = LatLng(position.latitude, position.longitude);
      });

      BitmapDescriptor icon = await _createMarkerImageFromAsset('posters/current_location_2.png', 100);

      _onAddMarker(
        position: _currentLocation, 
        icon: icon,
        id: '000001'
      );

      String address = await _getAddress(_currentLocation);
    
      setState(() {
        this.currentAddress = address;
      });
    } catch (error) {
      print(error);
    }
  }

  Future<double> calculateDistance({
    LatLng startPoint,
    LatLng destinationPoint,
  }) async {
    try {
      double distanceInMeters = await Geolocator().distanceBetween(
        startPoint.latitude,
        startPoint.longitude,
        destinationPoint.latitude,
        destinationPoint.longitude,
      );

      return distanceInMeters;
    } catch(error) {
      print(error);
    }
  }


  // add a marker on map
  void _onAddMarker({ LatLng position, BitmapDescriptor icon , String id}) {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(id),
        position: position,
        icon: icon,
      ));
    });
  }


  // draw direction on map
  _createPolylines(LatLng start, LatLng destination) async {
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polylines
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyBWRWyXqjkR9bW0Hz3Ld0qYbSlj0wjiebI", // Google Maps API Key
      PointLatLng(_currentLocation.latitude, _currentLocation.longitude),
      PointLatLng(_destination.latitude, _destination.longitude),
      travelMode: TravelMode.transit,
    );

    if (result.points.isNotEmpty) {
      polylineCoordinates = new List<LatLng>();
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('01');

    Polyline polyline = Polyline(
      polylineId: id,
      color: myColors.mainColor,
      points: polylineCoordinates,
      width: 10,
    );

    polylines[id] = polyline;

    setState(() {
      setPolylines = Set<Polyline>.of(polylines.values);
    });

    _onUpdateCamera();
  }


  // get address name by location
  Future<String> _getAddress(LatLng position) async {
    try {
      // Places are retrieved using the coordinates
      List<Placemark> p = await geolocator.placemarkFromCoordinates(position.latitude, position.longitude);

      // Taking the most probable result
      Placemark place = p[0];

      String address = "${place.subThoroughfare == "" ? "" : place.subThoroughfare + ", "}${place.thoroughfare  == "" ? "" : place.thoroughfare + ", "}${place.subAdministrativeArea  == "" ? "" : place.subAdministrativeArea + ", "}${place.administrativeArea}";

      return address;
    } catch (e) {
      print(e);
    }
  }

  _onGetInfomationOfRoute(context) async {
    if (BookingScreen.searchLocation == null && this._destination == null) {
      ToastHelper().showTopShortToastError('Vui lòng chọn điểm đến');
      return;
    }

    setState(() {
      _destination = BookingScreen.searchLocation == null ?   _destination : BookingScreen.searchLocation; 
    });

    await _createPolylines(_currentLocation, _destination);
    String startingAddress = await _getAddress(_currentLocation);
    String destinationAddress = await _getAddress(_destination);
    double distance = await calculateDistance(startPoint: _currentLocation, destinationPoint: _destination);

    setState(() {
      this.currentAddress = startingAddress;
      this.destinationAddress = destinationAddress;
      this.distance = distance;
    });
    

    BitmapDescriptor icon = await _createMarkerImageFromAsset('posters/destination_2.png', 200);

    _onAddMarker(
      position: _destination,
      icon: icon,
      id: '000002'
    );
    // calculate distance and price
    // show bottom dialog
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(20),
          topRight: const Radius.circular(20)
        ),
      ),
      barrierColor: Colors.black.withOpacity(0),
      context: context,
      builder: (BuildContext bc){
        return Container(
          height: 230,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
          decoration: BoxDecoration(
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20)
            ),
            boxShadow: [
              BoxShadow(
                blurRadius: 20,
                color: Colors.grey,
                spreadRadius: 1
              )
            ],
            color: myColors.whiteBackground
          ),
          child: Column(
            children: [
              SizedBox(height: 10,),
              Text(
                '${((this.distance).round() / 667).round()} Phút (${((this.distance).round() / 1000) } km)',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: myColors.mainColor
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Giá:',
                    style: TextStyle(
                      fontSize: 18,
                      color: myColors.placeholderText
                    ),
                  ),
                  SizedBox(width: 10,),
                  Text(
                    _getPriceAsString( 12 * this.distance ),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: myColors.accentColor,
                      fontSize: 20
                    ),
                  ),
                  SizedBox(width: 10,),
                  Text(
                    '(VND)',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: myColors.placeholderText,
                      fontSize: 25
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text('Xuất phát:',
                    style: TextStyle(
                      fontSize: 18,
                      color: myColors.placeholderText
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Text(
                      this.currentAddress,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: myColors.placeholderText,
                        fontSize: 18,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text('Điểm đến:',
                    style: TextStyle(
                      fontSize: 18,
                      color: myColors.placeholderText
                    ),
                  ),
                  SizedBox(width: 10,),
                  Flexible(
                    child: Text(
                      this.destinationAddress,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: myColors.placeholderText,
                        fontSize: 18,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  )
                ],
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2 - 10,
                    padding: EdgeInsets.all(10),
                    height: 80,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text(
                          'Hủy chuyến',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),
                        ),
                      ),
                      color: isBooked ?  myColors.accentColor : Colors.grey[400],
                      onPressed: (isBooking) ? null : () async {
                        showProgressDialog(loadingText: 'Đang hủy chuyến');
                        await _onCancel(context);
                        dismissProgressDialog();
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2 - 10,
                    padding: EdgeInsets.all(10),
                    height: 80,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text(
                          isBooked ? 'Danh sách tài xế' : 'Đặt xe',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),
                        ),
                      ),
                      color: isBooking ? Colors.grey[400] : myColors.mainColor,
                      onPressed: isBooking ? null : () async {
                        setState(() {
                          isBooking = true;
                        });
                        showProgressDialog(loadingText: 'Đang xử lý');
                        _onBooking(context);
                        dismissProgressDialog();
                      },
                    ),
                  )
                ],
              )
            ],
          ),
        );
      }
    );
  }

  String _getPriceAsString(double price) {
    int temp = (price).round();
    
    String priceAsString = '';

    while(temp >= 1000) {
      temp = (temp / 1000).toInt();

      priceAsString = priceAsString + '.000';
    }

    priceAsString = '${temp}${priceAsString}';

    return priceAsString;
  }

  _onBooking(context) async {
    if (isBooked) {

      if (this.listDriverNearLocation == null || this.listDriverNearLocation.length == 0) {
        dynamic listDriver = await DriversService().getDriverNearLocation(
          distance: 2000,
          lat: _currentLocation.latitude,
          lng: _currentLocation.longitude,
          limit: '10',
          offset: '0',
        );

        if (listDriver == null || listDriver.length == 0) {
          ToastHelper().showTopShortToastError('Không có tài xế gần đây');
          setState(() {
            isBooking = false;       
          });
          return;
        }

        setState(() {
          this.listDriverNearLocation = listDriver;
        });

      }
      Navigator.of(context).pop();
      _showListDrivers(context, this.listDriverNearLocation);

      setState(() {
        isBooking = false;     
      });

      return;
    }

    dynamic booking = await VehicleBookingsService().createBooking(
      startingPoint: new MyPosition(
        lat: _currentLocation.latitude,
        lng: _currentLocation.longitude,
      ),
      destination: new MyPosition(
        lat: _destination.latitude,
        lng: _destination.longitude
      ),
      distances: this.distance,
      price: 12 * this.distance,
      startAddress: this.currentAddress,
      destinationAddress: this.destinationAddress
    );

    if (booking == null || booking['vehicleBooking'] == null) {
      setState(() {
        isBooking = false;        
      });

      return;
    }

    setState(() {
      this.booking = booking['vehicleBooking'];
      this.isBooked = true;
    });

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userID = sharedPreferences.getString('userID');
    sharedPreferences.setString("bookingID_" + userID, this.booking.vehicleBookingID);

    Navigator.of(context).pop();
    _showListDrivers(context, booking['drivers']['list']);
    setState(() {
      this.listDriverNearLocation = booking['drivers']['list'];
    });

    setState(() {
      this.isBooking = false;
    });
  }

  _initState() async {
    if (MyGlobalVariable.currentBooking != null) {
      setState(() {
        this.booking = MyGlobalVariable.currentBooking;
      });
    }

    if (MyGlobalVariable.listDrivers != null) {
      setState(() {
        this.listDriverNearLocation = MyGlobalVariable.listDrivers;
      });
    }

    if (this.booking != null) {
      setState(() {
        this._currentLocation = new LatLng(this.booking.startingPoint.lat, this.booking.startingPoint.lng);
        this._destination = new LatLng(this.booking.destination.lat, this.booking.destination.lng);
        this.currentAddress = this.booking.startAddress;
        this.destinationAddress = this.booking.destinationAddress;
        this._textController.text = this.booking.destinationAddress;
        this.distance = this.booking.distances;
        this.isBooked = true;
      });

      _onGetInfomationOfRoute(context);
    }
  }

  _onCancel(context) async {
    if (!isBooked || booking == null) {
      return;
    }

    dynamic res = await VehicleBookingsService().cancelBooking(bookingID: booking.vehicleBookingID);

    if(res == null) {
      return;
    }

    MyGlobalVariable.currentBooking = null;
    MyGlobalVariable.listDrivers = null;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userID = sharedPreferences.getString('userID');
    sharedPreferences.remove('bookingID_'+userID);

    setState(() {
      Navigator.of(context).pop();
      isBooked = false;
      booking = null;
      listDriverNearLocation = null;
    });
  }

  _showListDrivers(context, drivers) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          width: MediaQuery.of(context).size.width,
          height: 400,
          margin: EdgeInsets.fromLTRB(20, 40, 20 , 40),
          decoration: BoxDecoration(
            color: myColors.whiteBackground,
            borderRadius: BorderRadius.circular(20)
          ),
          child: Column(
            children: [
              Container(
                height: 60,
                decoration: BoxDecoration(
                ),
                child: Center(
                  child: Text(
                    'Tài xế trong khu vực',
                    style: TextStyle(
                      fontSize: 25,
                      color: myColors.mainColor,
                      inherit: false
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 400.0 - 60,
                padding: EdgeInsets.all(10),
                child: ListView.builder(
                  itemBuilder: (context,index){
                    return Card(
                      child: InkWell(
                        onTap: () {
                          _onSelectDriver(context, drivers[index]);
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: myColors.mainColor,
                                  shape: BoxShape.circle
                                ),
                              ),
                              SizedBox(width: 10,),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  // mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      drivers[index]['userID']['lastName'],
                                      style: TextStyle(
                                        fontSize: 25.0,
                                        color: Colors.black
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Đánh giá:',
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: myColors.placeholderText,
                                            inherit: false
                                          ),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[400],
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[400],
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[400],
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[400],
                                        ),
                                        Icon(
                                          Icons.star_border_outlined,
                                          color: Colors.yellow[400],
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    );
                  },
                  itemCount: drivers.length,
                ),
              ),
              RaisedButton(
                shape: CircleBorder( 
                  side: BorderSide(
                      color: myColors.accentColor,
                      width: 1,
                  )
                ),
                splashColor: Colors.grey,
                padding: EdgeInsets.all(0),
                color: myColors.whiteBackground,
                child: Container(
                  height: 65,
                  width: 65,
                  decoration: BoxDecoration(
                    
                  ),
                  child: Center(
                    child: Icon(
                      Icons.close,
                      color: myColors.accentColor
                    )
                  ),
                ),
                onPressed: () {
                  dismissProgressDialog();
                  Navigator.of(context).pop();
                },
              )
            ],
          )
        );
      }
    );
  }

  _onSelectDriver(context, driver) async {
    double rating = driver['rating'];
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height - 200,
          margin: EdgeInsets.fromLTRB(20, 40, 20 , 40),
          decoration: BoxDecoration(
            color: myColors.whiteBackground,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 20,),
                  Center(
                    child: Text(
                      'Thông tin tài xế',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: myColors.mainColor,
                        inherit: false,
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: myColors.accentColor
                    ),
                    child: Center(
                      child: Text(
                        'TH',
                        style: TextStyle(
                          color: myColors.whiteText,
                          fontSize: 80,
                          fontWeight: FontWeight.bold,
                          inherit: false,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Text(
                    driver['userID']['firstName'] + ' ' + driver['userID']['lastName'],
                    style: TextStyle(
                      inherit: false,
                      fontSize: 25,
                      color: myColors.mainColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 30,),
                      Text(
                        '22 tuổi',
                        style: TextStyle(
                          inherit: false,
                          fontSize: 20,
                          color: myColors.placeholderText,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 30,),
                      Text(
                        'Quê quán: Quảng Bình',
                        style: TextStyle(
                          inherit: false,
                          fontSize: 20,
                          color: myColors.placeholderText,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 30,),
                      Text(
                        'Đánh giá:',
                        style: TextStyle(
                          fontSize: 20,
                          color: myColors.placeholderText,
                          inherit: false
                        ),
                      ),
                      Icon(
                        rating >= 1 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                      ),
                      Icon(
                        rating >= 2 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                      ),
                      Icon(
                        rating >= 3 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                      ),
                      Icon(
                        rating >= 4 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                      ),
                      Icon(
                        rating >= 5 ? Icons.star : Icons.star_border_outlined,
                        color: Colors.yellow[400],
                      )
                    ],
                  ),
                ],
              ),
              
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2 - 20,
                    padding: EdgeInsets.all(10),
                    height: 80,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: Text(
                          'Trở về',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),
                        ),
                      ),
                      color:  myColors.accentColor,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2 - 20,
                    padding: EdgeInsets.all(10),
                    height: 80,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: Text(
                          'Kết nối',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),
                        ),
                      ),
                      color:  myColors.mainColor,
                      onPressed: () {
                        MyGlobalVariable.currentBooking = this.booking;
                        MyGlobalVariable.listDrivers = this.listDriverNearLocation;
                        VehicleBookingsService().selectDriver(
                          bookingID: booking.vehicleBookingID,
                          driverID: driver['driverID']
                        ).then((value) {
                          MyGlobalVariable.currentBooking = value;
                        });
                        
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => JourneyScreen(booking: booking)));
                      },
                    ),
                  )
                ],
              )
              )
            ],
          )
        );
      },
    );
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  Future <BitmapDescriptor> _createMarkerImageFromAsset(String iconPath, int width) async {
    Uint8List markerIcon = await getBytesFromAsset(iconPath, width);
    BitmapDescriptor bitmapImage = BitmapDescriptor.fromBytes(markerIcon);
    return bitmapImage;
  }
}