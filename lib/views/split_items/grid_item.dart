import 'package:booking_grab_apis/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GridItem extends StatelessWidget {
  Function onTap;
  String title;

  GridItem({ this.title, this.onTap });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 42) / 3,
      padding: EdgeInsets.all(5),
      child: InkWell(
        onTap: onTap,
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(
                color: myColors.mainColor,
                fontSize: 20.0
              ),
            ),
            SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                border: Border.all(
                  color: myColors.mainColor,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(10)
              ),
            )
          ],
        ),
      )
    );
  }
  
}