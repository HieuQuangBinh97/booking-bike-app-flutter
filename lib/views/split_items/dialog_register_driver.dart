import 'dart:io';
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/models/drivers.dart';
import 'package:booking_grab_apis/services/drivers.service.dart';
import 'package:booking_grab_apis/views/driver_screen.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class DialogRegisterDriver extends StatefulWidget {
  BuildContext context;

  @override
  State<StatefulWidget> createState() => _dialogRegisterDriver();
  
}

class _dialogRegisterDriver extends State<DialogRegisterDriver> {
  bool agree = false;
  File frontOfIdentifyCardImage;
  File backOfIdentifyCardImage;
  final picker = ImagePicker();
  TextEditingController _identityCardNumberController = new TextEditingController();
  String _identityCardNumberError;
  bool isRegisting = false; 
  DriversService driversService = new DriversService();
  Color warningColor = Colors.grey;

  @override
  void setState(fn) {
    super.setState(fn);
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Trở thành tài xế',
          style: TextStyle(
            fontSize: 25
          ),
        ),
        backgroundColor: myColors.mainColor,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left,
            size: 40,
          ),
          onPressed: (){
            Navigator.of(context).pop();
          }
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
        decoration: BoxDecoration(
          color: myColors.whiteBackground,
          borderRadius: BorderRadius.circular(20),
        ),
        child: ListView(
          children: [
            const SizedBox(height: 10,),
            Align(
              alignment: Alignment.centerLeft,
              child: Text('Số CMND/Căn cước',
                style: TextStyle(
                  fontSize: 20,
                  color: myColors.placeholderText
                ),
              ),
            ),
            const SizedBox(height: 10,),
            Container(
              decoration: BoxDecoration(
                color: myColors.whiteBackground,
              ),
              child: TextField(
                style: TextStyle(
                  fontSize: 20,
                ),
                textAlign: TextAlign.left,
                enabled: !isRegisting,
                controller: _identityCardNumberController,
                decoration: InputDecoration(
                  labelText: 'Số CMND',
                  contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: myColors.mainColor, 
                      width: 1
                    ),
                    borderRadius:  BorderRadius.all(Radius.circular(6))
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30,),
            Align(
              alignment: Alignment.centerLeft,
              child: Text('Ảnh mặt trước CMND/Căn cước',
                style: TextStyle(
                  fontSize: 20,
                  color: myColors.placeholderText
                ),
              ),
            ),
            const SizedBox(height: 10,),
            Align(
              alignment: Alignment.centerLeft,
              child: frontOfIdentifyCardImage == null ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: myColors.placeholderText.withOpacity(0.5),
                ),
                height: 200,
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: IconButton(
                  alignment: Alignment.center,
                  icon: Icon(
                    Icons.add_a_photo,
                    color: myColors.whiteText,
                    size: 40,
                  ),
                  onPressed: isRegisting ? null : getFrontOfIdentifyCardImage,
                ),
              ) : Container(
                height: 200,
                child: Stack(
                  children: [
                    Image.file(
                      frontOfIdentifyCardImage,
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.fitWidth,
                    ),
                    FloatingActionButton(
                      backgroundColor: Colors.black.withOpacity(0.4),
                      child: Icon(
                        Icons.edit_outlined,
                        color: myColors.whiteText,
                      ),
                      onPressed: isRegisting ? null : getFrontOfIdentifyCardImage,
                    )
                  ],
                ),
              )
            ),
            const SizedBox(height: 20,),
            Align(
              alignment: Alignment.centerLeft,
              child: Text('Ảnh Mặt Sau CMND/Căn cước',
                style: TextStyle(
                  fontSize: 20,
                  color: myColors.placeholderText
                ),
              ),
            ),
            const SizedBox(height: 10,),
            Align(
              alignment: Alignment.centerLeft,
              child: backOfIdentifyCardImage == null ? Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: myColors.placeholderText.withOpacity(0.5),
                ),
                height: 200,
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: IconButton(
                  alignment: Alignment.center,
                  icon: Icon(
                    Icons.add_a_photo,
                    color: myColors.whiteText,
                    size: 40,
                  ),
                  onPressed: isRegisting ? null : getBackOfIdentifyCardImage,
                ),
              ) : Container(
                height: 200,
                child: Stack(
                  children: [
                    Image.file(
                      backOfIdentifyCardImage,
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.fitWidth,
                    ),
                    FloatingActionButton(
                      backgroundColor: Colors.black.withOpacity(0.4),
                      child: Icon(
                        Icons.edit_outlined,
                        color: myColors.whiteText,
                      ),
                      onPressed: isRegisting ? null : getBackOfIdentifyCardImage,
                    )
                  ],
                ),
              )
            ),
            const SizedBox(height: 30,),
            Align(
              alignment: Alignment.centerLeft,
              child: Text('Điều khoản',
                style: TextStyle(
                  fontSize: 20,
                  color: myColors.placeholderText
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text('Để trở thành tài xế của GB, bạn phải cung cấp số chứng minh nhân dân,mờ thông tin',
                  style: TextStyle(
                    fontSize: 20,
                    color: myColors.placeholderText
                  ),
                ),
              )
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Checkbox(
                    value: agree,
                    onChanged: (bool value) {
                      setState(() {
                        agree = value;
                        warningColor = Colors.grey;
                      });
                    },
                    checkColor: myColors.mainColor,
                  ),
                  SizedBox(width: 10,),
                  Text(
                    'Đồng ý',
                    style: TextStyle(
                      fontSize: 20,
                      color: warningColor
                    ),
                  ),
                ],
              ),
            ),
            RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              padding: EdgeInsets.all(0),
              color: myColors.mainColor,
              child: Container(
                height: 65,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: myColors.mainColor,
                ),
                child: Center(
                  child: Text('Đăng ký',
                    style: TextStyle(
                      color: myColors.whiteText,
                      fontSize: 20
                    ),
                  ),
                ),
              ),
              onPressed: isRegisting ? null : () {
                _onRegisterDriver(context);
              }
            )
          ],
        ),
      )
    );
  }

  _onRegisterDriver(context) async {
    if (isRegisting) {
      return;
    }

    if (!agree) {
      setState(() {
        warningColor = Colors.red;
      });

      return;
    }

    if (_identityCardNumberController.text == "" || _identityCardNumberController.text == null) {
      setState(() {
        isRegisting = false;
        _identityCardNumberError = 'Vui lòng điền số CMND/Căn cước';
      });

      return;
    }

    if (frontOfIdentifyCardImage == null || backOfIdentifyCardImage == null) {
      setState(() {
        isRegisting = false;
      });

      return;
    }

    setState(() {
      isRegisting = true;
    });

    Drivers newDriver = await driversService.registerDriver(
      identityCardNumber: _identityCardNumberController.text,
      backOfIdentityCard: backOfIdentifyCardImage,
      frontOfIdentityCard: frontOfIdentifyCardImage
    );

    if (newDriver == null) {
      setState(() {
        isRegisting = false;
      });

      return;
    }

    setState(() {
      isRegisting = false;
    });

    Navigator.of(context).pop();
  }

  Future getFrontOfIdentifyCardImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        frontOfIdentifyCardImage = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getBackOfIdentifyCardImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        backOfIdentifyCardImage = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

}