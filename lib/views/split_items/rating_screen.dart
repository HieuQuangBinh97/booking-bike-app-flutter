import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/services/drivers.service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RatingScreen extends StatefulWidget {
  String driverID;
  RatingScreen({ this.driverID });
  @override
  _ratingScreen createState() => _ratingScreen(driverID: this.driverID);
}

class _ratingScreen extends State<RatingScreen> {
  var myFeedbackText = "COULD BE BETTER";
  var sliderValue = 0.0;
  String driverID;

  int score=87;
  IconData myFeedback1= FontAwesomeIcons.star,myFeedback2= FontAwesomeIcons.star,myFeedback3= FontAwesomeIcons.star,
      myFeedback4= FontAwesomeIcons.star,myFeedback5 = FontAwesomeIcons.star;
  Color myFeedbackColor1 = Colors.grey,myFeedbackColor2 = Colors.grey,myFeedbackColor3 = Colors.grey,
      myFeedbackColor4 = Colors.grey,myFeedbackColor5 = Colors.grey;

  _ratingScreen({
    this.driverID
  });

  void showTopShortToast() {
    Fluttertoast.showToast(
        msg: "Vui lòng đánh giá",
        textColor: Colors.black,
        backgroundColor: Colors.blue,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 4,
        fontSize:20
    );
  }


  Color getPointColor(int score){
    if(score >= 80){
      return Colors.green.withOpacity(0.8);
    }
    if(score<80 && score >=50){
      return Colors.blue.withOpacity(0.8);
    }
    if(score<50){
      return Colors.red.withOpacity(0.8);
    }

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: myColors.mainColor,
        leading: Container(
          decoration: BoxDecoration(
          ),
          alignment: Alignment.center,
          child: IconButton(
            icon: Icon(
              Icons.keyboard_arrow_left,
              size: 40,
            ),
            onPressed: (){
              Navigator.of(context).pop();
            }
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Column(
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.width/5,),
              Center(
                child: Text("Đánh giá tài xế", style: TextStyle(
                  fontSize: 30,
                ),),
              ),
              SizedBox(height:MediaQuery.of(context).size.height/20,),
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Column(children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Container(
                                  child: Container(
                                    child:Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: StarWidget(),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 3),
                                  child: Container(child: Slider(
                                    min: 0.0,
                                    max: 5.0,
                                    divisions: 5,
                                    value: sliderValue,
                                    activeColor: Colors.white.withOpacity(0),
                                    inactiveColor: Colors.white.withOpacity(0),
                                    onChanged: (newValue) {
                                      setState(() {
                                        sliderValue = newValue;
                                        if (sliderValue >= 1.0 ) {
                                          myFeedback1 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor1 = Colors.yellow;
                                        }
                                        else if (sliderValue < 1.0 ){
                                          myFeedback1 = FontAwesomeIcons.star;
                                          myFeedbackColor1 = Colors.grey;

                                        }
                                        if (sliderValue >= 2.0 ) {
                                          myFeedback2 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor2= Colors.yellow;
                                        }
                                        else if (sliderValue < 2.0 ){
                                          myFeedback2 = FontAwesomeIcons.star;
                                          myFeedbackColor2 = Colors.grey;

                                        }
                                        if (sliderValue >= 3.0 ) {
                                          myFeedback3 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor3 = Colors.yellow;
                                        }
                                        else if (sliderValue < 3.0 ){
                                          myFeedback3 = FontAwesomeIcons.star;
                                          myFeedbackColor3 = Colors.grey;

                                        }
                                        if (sliderValue >= 4.0 ) {
                                          myFeedback4 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor4 = Colors.yellow;
                                        }
                                        else if (sliderValue < 4.0 ){
                                          myFeedback4 = FontAwesomeIcons.star;
                                          myFeedbackColor4 = Colors.grey;

                                        }
                                        if (sliderValue >= 5.0 ) {
                                          myFeedback5 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor5 = Colors.yellow;
                                        }
                                        else if (sliderValue < 5.0 ){
                                          myFeedback5 = FontAwesomeIcons.star;
                                          myFeedbackColor5 = Colors.grey;
                                        }
                                      });
                                    },
                                  ),),
                                ),
                              ],
                            ),
                            SizedBox(height:MediaQuery.of(context).size.height/20,),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                  ),
                                  padding: EdgeInsets.all(0),
                                  child: Container(
                                    width: 200,
                                    height: 60,
                                    decoration: BoxDecoration(
                                      color: myColors.mainColor,
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: 10,
                                          spreadRadius: 2,
                                        )
                                      ],
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Gửi đánh giá',
                                        style: TextStyle(
                                          color: myColors.whiteText,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                  onPressed: ()  async{
                                    print(sliderValue.toInt());
                                    int numstar=sliderValue.toInt();

                                    if(numstar==0){
                                      showTopShortToast();
                                      return;
                                    }

                                    DriversService().rating(
                                      driverID: this.driverID,
                                      rating: numstar
                                    ).then((value) {
                                      if (value) {
                                        Navigator.of(context).pop();
                                      }
                                    });

                                    
                                  },
                                )
                              ),
                            ),
                          ],)
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> StarWidget(){
    List<Widget> myContainer = new List();
    myContainer.add(Container(child: Icon(
      myFeedback1, color: myFeedbackColor1, size: 50.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback2, color: myFeedbackColor2, size: 50.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback3, color: myFeedbackColor3, size: 50.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback4, color: myFeedbackColor4, size: 50.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback5, color: myFeedbackColor5, size: 50.0,)));
    return myContainer;
  }
  Widget _myAppBar() {
    return Container(
      height: 70.0,
      width: MediaQuery
          .of(context)
          .size
          .width,

      decoration: BoxDecoration(
        gradient: new LinearGradient(
          colors: [
            const Color(0xff662d8c),
            const Color(0xffed1e79),
          ],
          begin: Alignment.centerRight,
          end: new Alignment(-1.0, -1.0),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child:Container(child:IconButton(
                      icon: Icon(FontAwesomeIcons.arrowLeft,color: Colors.white,), onPressed: () {
                    //
                  }),),),
                Expanded(
                  flex: 5,
                  child:Container(child:Text('Rate', style:
                  TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 22.0
                  ),),),),
                Expanded(
                  flex: 1,
                  child:Container(child:IconButton(
                      icon: Icon(FontAwesomeIcons.star,color: Colors.white,), onPressed: () {
                    //
                  }),),),
              ],)
        ),
      ),
    );
  }
}

