import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/models/notifications.dart';
import 'package:booking_grab_apis/models/users.dart';
import 'package:booking_grab_apis/services/socket.service.dart';
import 'package:booking_grab_apis/services/users.service.dart';
import 'package:booking_grab_apis/validators/form_validator.dart';
import 'package:booking_grab_apis/views/login_screen.dart';
import 'package:booking_grab_apis/views/split_items/notification_item.dart';
import 'package:booking_grab_apis/views/split_items/waiting_load_profile.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _profileScreen createState() => _profileScreen();
  
}

class _profileScreen extends State<ProfileScreen> with SingleTickerProviderStateMixin {
  SharedPreferences sharedPreferences;
  Users profile;
  TextEditingController _firstNameController = new TextEditingController();
  TextEditingController _lastNameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _newPasswordController = new TextEditingController();
  TextEditingController _oldPasswordController = new TextEditingController();
  TextEditingController _confirmPasswordController = new TextEditingController();
  TextEditingController _phoneNumberController = new TextEditingController();

  String _firstNameError;
  String _emailError;
  String _phoneNumberError;
  String _newPasswordError;
  String _oldPasswordError;
  String _confirmPasswordError;

  bool checkingForm = false;
  bool isRegistering = false;
  bool isUpdatingProfile = false;
  bool isLoading = true;
  bool isChangingPass = false;

  AnimationController animationControllerOne;
  Animation<Color> animationOne;
  Animation<Color> animationTwo;

  UsersService usersService;

  @override
  void initState() {
    usersService = new UsersService();
    _loadProfile();
    super.initState();

    animationControllerOne = AnimationController(
        duration: Duration(milliseconds: 2000),
        vsync: this
    );
    animationOne = ColorTween(begin: Colors.grey,end: Colors.white70).animate(animationControllerOne);
    animationTwo = ColorTween(begin: Colors.white70,end: Colors.grey).animate(animationControllerOne);
    animationControllerOne.forward();
    animationControllerOne.addListener((){
      if(animationControllerOne.status == AnimationStatus.completed){
        animationControllerOne.reverse();
      } else if(animationControllerOne.status == AnimationStatus.dismissed){
        animationControllerOne.forward();
      }

      setState(() {});
    });
  }

  @override
  void dispose() {
    if (animationControllerOne != null) {
      animationControllerOne.dispose();
    }
    super.dispose();
    print('stop animation');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading? _buildWaitingShaderMask() : Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: myColors.whiteBackground
        ),
        child: ListView(
          children: [
            Container(
              height: 225,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 150,
                      decoration: BoxDecoration(
                        // image:  DecorationImage(
                        //   image: AssetImage('banner.png'),
                        //   fit: BoxFit.cover,
                        // ),
                        color: myColors.mainColor,
                        borderRadius: BorderRadius.circular(20)
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Text(
              '${profile.firstName} ${profile.lastName}',
              style: TextStyle(
                color:  Colors.black,
                fontWeight: FontWeight.w600,
                fontFamily: 'Roboto',
                fontSize: 40,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 10,),
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        'Chi tiết',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      new Icon(
                          Icons.email,
                          size: 20,
                          color: myColors.placeholderText,
                      ),
                      SizedBox(width: 3,),
                      Text(
                        'Email:',
                        style: TextStyle(
                          fontSize: 20,
                          color: myColors.placeholderText,
                        ),
                      ),
                      SizedBox(width: 5,),
                      Text(
                        '${profile.email}',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      new Icon(
                          Icons.phone,
                          size: 20,
                          color: myColors.placeholderText,
                      ),
                      SizedBox(width: 3,),
                      Text(
                        'Số ĐT:',
                        style: TextStyle(
                          fontSize: 20,
                          color: myColors.placeholderText,
                        ),
                      ),
                      SizedBox(width: 5,),
                      Text(
                        '${profile.phoneNumber}',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                      )
                    ],
                  ),
                  Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Icon(
                            Icons.home,
                            size: 20,
                            color: myColors.placeholderText,
                        ),
                        SizedBox(width: 3,),
                        Text(
                          'Địa chỉ:',
                          style: TextStyle(
                            fontSize: 20,
                            color: myColors.placeholderText,
                          ),
                        ),
                        SizedBox(width: 5,),
                        Flexible(
                          child: Text(
                            'Quảng Bình',
                            softWrap: true,
                            textWidthBasis: TextWidthBasis.parent,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                            ),
                          )
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 30, 20, 10),
                      height: 50,
                      decoration: BoxDecoration(
                        color: myColors.mainColor.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Center(
                        child: Text(
                          'Chỉnh sửa thông tin cá nhân',
                          style: TextStyle(
                            fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                       _showChangeProfileDialog(context);
                    }
                  ),
                  const Divider(
                    color: Colors.grey,
                    height: 20,
                    thickness: 0.4,
                    indent: 10,
                    endIndent: 10,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Text(
                        'Bảo mật & đăng nhập',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.grey[400],
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Center(
                        child: Text(
                          'Đổi mật khẩu',
                          style: TextStyle(
                            fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      print('on tap');
                      _showDialogChangePassword(context);
                    }
                  ),
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                      height: 50,
                      decoration: BoxDecoration(
                        color: myColors.accentColor,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Center(
                        child: Text(
                          'Đăng xuất',
                          style: TextStyle(
                            fontSize: 20,
                            color: myColors.whiteText
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      _logout();
                       Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginScreen()), (Route<dynamic> route) => false);
                    }
                  ),
                ],
              ),
            )
          ],
        )
      )
    );
  }

  _logout() async {
    sharedPreferences = await SharedPreferences.getInstance();
    this.sharedPreferences.remove('token');
    this.sharedPreferences.remove('userID');
    MyGlobalVariable.currentUser = null;
    MyGlobalVariable.currentBooking = null;
    MyGlobalVariable.listDrivers = null;
    MyGlobalVariable.notifications = new List<Notifications>();
    SocketService.socket.emit('disconnected');
  }

  _showChangeProfileDialog(context) {
    _firstNameController.text = profile.firstName;
    _lastNameController.text = profile.lastName;
    _emailController.text = profile.email;
    try {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Scaffold(
            backgroundColor: Colors.black.withOpacity(0.3),
            appBar: AppBar(
              backgroundColor: myColors.mainColor,
              title: Text(
                'Chỉnh sửa thông tin cá nhân',
                style: TextStyle(
                  color:  myColors.whiteText,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Roboto',
                  fontSize: 25,
                ),
                textAlign: TextAlign.center,
              ),
              leading: IconButton(
                icon: new Icon(
                  Icons.keyboard_arrow_left,
                  color: myColors.whiteText,
                ),
                color: myColors.whiteText,
                iconSize: 30,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            body: Container(
              margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: ListView(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: myColors.whiteBackground,
                    ),
                    child: Column(
                      children: [
                        const SizedBox(height: 30,),
                        Container(
                          decoration: BoxDecoration(
                            color: myColors.whiteBackground,
                          ),
                          child: TextField(
                            style: TextStyle(
                              fontSize: 20,
                            ),
                            enabled: !isRegistering,
                            textAlign: TextAlign.left,
                            controller: _firstNameController,
                            onChanged: (String value) {
                              String error = FormValidator().checkFirstName(value);
                              checkingForm = true;
                              setState(() {
                                _firstNameError = error;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: 'FirstName',
                              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                              errorText: checkingForm ? _firstNameError : null,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: myColors.mainColor, 
                                  width: 1
                                ),
                                borderRadius:  BorderRadius.all(Radius.circular(6))
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 30,),
                        Container(
                          decoration: BoxDecoration(
                            color: myColors.whiteBackground,
                          ),
                          child: TextField(
                            style: TextStyle(
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.left,
                            enabled: !isRegistering,
                            controller: _lastNameController,
                            decoration: InputDecoration(
                              labelText: 'LastName',
                              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: myColors.mainColor, 
                                  width: 1
                                ),
                                borderRadius:  BorderRadius.all(Radius.circular(6))
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 30,),
                        Container(
                          decoration: BoxDecoration(
                            color: myColors.whiteBackground,
                          ),
                          child: TextField(
                            style: TextStyle(
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.left,
                            enabled: !isRegistering,
                            onChanged: (String value) {
                              String error = FormValidator().checkEmail(value);
                              checkingForm = true;
                              setState(() {
                                _emailError = error;
                              });
                            },
                            controller: _emailController,
                            decoration: InputDecoration(
                              errorText: checkingForm ? _emailError : null,
                              labelText: 'Email',
                              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: myColors.mainColor, 
                                  width: 1
                                ),
                                borderRadius:  BorderRadius.all(Radius.circular(6))
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 30,),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text('Avartar',
                            style: TextStyle(
                              fontSize: 20,
                              color: myColors.placeholderText
                            ),
                          ),
                        ),
                        Card(
                          child: InkWell(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: myColors.placeholderText.withOpacity(0.5),
                                ),
                                height: 200,
                                alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                child: IconButton(
                                  alignment: Alignment.center,
                                  icon: Icon(
                                    Icons.add_a_photo,
                                    color: myColors.whiteText,
                                    size: 40,
                                  ),
                                  onPressed: null,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            splashColor: Colors.grey,
                            
                            color: myColors.mainColor,
                            child: Container(
                              height: 65,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Center(
                                child: Text('Save',
                                  style: TextStyle(
                                    color: myColors.whiteText,
                                    fontSize: 20
                                  ),
                                ),
                              ),
                            ),
                            onPressed: () {
                              _onUpdateProfile();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ) ,
            ),
          );
        }
      );
    } catch(error) {

    }
  }

  _loadProfile() async {
    if (MyGlobalVariable.currentUser != null) {
      setState(() {
        profile = MyGlobalVariable.currentUser;
        isLoading = false;
        animationControllerOne.dispose();
      });

      return;
    }
    Users user = await usersService.getProfile();

    if (user != null) {
      setState(() {
        profile = user;
        MyGlobalVariable.currentUser = user;
        isLoading = false;
        animationControllerOne.dispose();
      });
    }
  }

  Widget _buildWaitingShaderMask() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
      child: SafeArea(
        child: ShaderMask(
          shaderCallback: (rect){
            return LinearGradient(
              tileMode: TileMode.clamp,
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [animationOne.value,animationTwo.value]).createShader(rect,textDirection: TextDirection.ltr
            );
          },
          child: SizedBox(
            height: MediaQuery.of(context).size.height-50,
            child: ListView(
              children: [
                Container(
                  height: 225,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 150,
                          decoration: BoxDecoration(
                            // image:  DecorationImage(
                            //   image: AssetImage('banner.png'),
                            //   fit: BoxFit.cover,
                            // ),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(color: Colors.grey)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: double.infinity,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)
                  ),
                ),
                SizedBox(height: 5,),
                Container(
                  width: double.infinity,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)
                  ),
                ),
                SizedBox(height: 5,),
                Container(
                  width: 40.0,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)
                  ),
                ),
              ],
            )
          )
        ),
      )
    );
  }

  _onUpdateProfile() async {
    try {
        if (isUpdatingProfile) {
        ToastHelper().showTopShortToastError('Đang cập nhật, vui lòng chờ!');
        return;
      }

      setState(() {
        isUpdatingProfile = true;
      });

      bool isValidFormData = _validateUpdateProfileForm();

      if (!isValidFormData) {
        setState(() {
          isUpdatingProfile = false;
        });
        return;
      }

      Map<String, dynamic> data = {
        
      };

      if (_firstNameController.text != null || _firstNameController.text != profile.firstName) {
        data['firstName'] = _firstNameController.text;
      }

      if (_lastNameController.text != null || _lastNameController.text != profile.firstName) {
        data['lastName'] = _lastNameController.text;
      }

      if (_emailController.text != null || _emailController.text != profile.firstName) {
        data['email'] = _emailController.text;
      }

      bool updatedUser = await this.usersService.updateProfile(data);

      if (!updatedUser) {
        setState(() {
          isUpdatingProfile = false;
        });
        ToastHelper().showTopShortToastError('Cập nhật không thành công');
      }

      setState(() {
        if (_firstNameController.text != null || _firstNameController.text != profile.firstName) {
          profile.firstName = _firstNameController.text;
        }

        if (_lastNameController.text != null || _lastNameController.text != profile.firstName) {
          profile.lastName = _lastNameController.text;
        }

        if (_emailController.text != null || _emailController.text != profile.firstName) {
          profile.email = _emailController.text;
        }

        MyGlobalVariable.currentUser = this.profile;
        isUpdatingProfile = false;
      });

    } catch(error) {
      setState(() {
        ToastHelper().showTopShortToastError('Cập nhật không thành công');
        isUpdatingProfile = false;
      });
    }

  }


  bool _validateUpdateProfileForm() {
    bool isValid = true;
    
    setState(() {
      checkingForm = true;
    });

    if (this._firstNameController.text != null && FormValidator().checkFirstName(this._firstNameController.text) != null) {
      setState(() {
        _firstNameError = FormValidator().checkFirstName(this._firstNameController.text);
      });

      isValid = false;
    }

    if (this._emailController.text != null && FormValidator().checkEmail(this._emailController.text) != null) {
      setState(() {
        _emailError = FormValidator().checkEmail(this._emailController.text);
      });

      isValid = false;
    }

    setState(() {
      checkingForm = false;
    });
    
    return isValid;
  }

  _showDialogChangePassword(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: myColors.mainColor,
              title: Text(
                'Đổi mật khẩu',
                style: TextStyle(
                  color:  myColors.whiteText,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Roboto',
                  fontSize: 25,
                ),
                textAlign: TextAlign.center,
              ),
              leading: IconButton(
                icon: new Icon(
                  Icons.keyboard_arrow_left,
                  color: myColors.whiteText,
                ),
                color: myColors.whiteText,
                iconSize: 30,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            backgroundColor: Colors.black.withOpacity(0.5),
            body: Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width - 20 ,
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.fromLTRB(10, 40, 10, 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: myColors.whiteBackground,
              ),
              child: ListView(
                children: [
                   Column(
                children: [
                  const SizedBox(height: 30,),
                  Container(
                    decoration: BoxDecoration(
                      color: myColors.whiteBackground,
                    ),
                    child: TextField(
                      style: TextStyle(
                        fontSize: 20,
                      ),
                      textAlign: TextAlign.left,
                      enabled: !isRegistering,
                      onChanged: (String value) {
                        String error = FormValidator().checkPassword(value);
                        checkingForm = true;
                        setState(() {
                          _oldPasswordError = error;
                        });
                      },
                      controller: _oldPasswordController,
                      obscureText: true,
                      decoration: InputDecoration(
                        errorText: checkingForm ? _oldPasswordError : null,
                        labelText: 'Mật khẩu cũ',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: myColors.mainColor, 
                            width: 1
                          ),
                          borderRadius:  BorderRadius.all(Radius.circular(6))
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30,),
                  Container(
                    decoration: BoxDecoration(
                      color: myColors.whiteBackground,
                    ),
                    child: TextField(
                      style: TextStyle(
                        fontSize: 20,
                      ),
                      textAlign: TextAlign.left,
                      enabled: !isRegistering,
                      onChanged: (String value) {
                        String error = FormValidator().checkPassword(value);
                        checkingForm = true;
                        setState(() {
                          _newPasswordError = error;
                        });
                      },
                      controller: _newPasswordController,
                      obscureText: true,
                      decoration: InputDecoration(
                        errorText: checkingForm ? _newPasswordError : null,
                        labelText: 'Mật khẩu mới',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: myColors.mainColor, 
                            width: 1
                          ),
                          borderRadius:  BorderRadius.all(Radius.circular(6))
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30,),
                  Container(
                    decoration: BoxDecoration(
                      color: myColors.whiteBackground,
                    ),
                    child: TextField(
                      style: TextStyle(
                        fontSize: 20,
                      ),
                      textAlign: TextAlign.left,
                      enabled: !isRegistering,
                      onChanged: (String value) {
                        String error = FormValidator().checkConfirmPassword(value);
                        checkingForm = true;
                        setState(() {
                          _confirmPasswordError = error;
                        });
                      },
                      controller: _confirmPasswordController,
                      obscureText: true,
                      decoration: InputDecoration(
                        errorText: checkingForm ? _confirmPasswordError : null,
                        labelText: 'Xác nhận mật khẩu mới',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: myColors.mainColor, 
                            width: 1
                          ),
                          borderRadius:  BorderRadius.all(Radius.circular(6))
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      splashColor: Colors.grey,
                      
                      color: myColors.mainColor,
                      child: Container(
                        height: 65,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Text('Save',
                            style: TextStyle(
                              color: myColors.whiteText,
                              fontSize: 20
                            ),
                          ),
                        ),
                      ),
                      onPressed: () {
                        print('on change pass');
                        _onChangePass();
                      },
                    ),
                  ),
                ]
              )
                ],
              )
            ),
          );
        }
      );
  }

  _onChangePass() async {
    try {
      if (isChangingPass) {
        ToastHelper().showTopShortToastError('Đang cập nhật, vui lòng chờ');
        return;
      }

      setState(() {
        isChangingPass = true;
      });

      if (!_validateChangePassForm()) {
        setState(() {
          isChangingPass = false;
        });

        return;
      }

      bool isUpdated = await this.usersService.changePassword(
        oldPassword: _oldPasswordController.text,
        newPassword: _newPasswordController.text,
        confirmPassword: _confirmPasswordController.text
      );

      if (!isUpdated) {
        setState(() {
          isChangingPass = false;
        });

        ToastHelper().showTopShortToastError('Cập nhật không thành công');
      }

      setState(() {
        isChangingPass = false;
      });

      ToastHelper().showTopShortToastSuccessfuly('Đổi mật khẩu thành công');

    } catch(error) {
      setState(() {
        isChangingPass = false;
      });

      ToastHelper().showTopShortToastError('Đã xảy ra lỗi');
    }
  }

  bool _validateChangePassForm() {
    bool isValid = true;
    
    setState(() {
      checkingForm = true;
    });

    if (this._oldPasswordController.text != null && FormValidator().validatePassword(this._oldPasswordController.text) != null) {
      setState(() {
        _oldPasswordError = FormValidator().validatePassword(this._oldPasswordController.text);
      });

      isValid = false;
    }

    if (this._newPasswordController.text != null && FormValidator().validatePassword(this._newPasswordController.text) != null) {
      setState(() {
        _newPasswordError = FormValidator().validatePassword(this._newPasswordController.text);
      });

      isValid = false;
    }

    if (this._confirmPasswordController.text != null && FormValidator().validateConfimPassWord(this._newPasswordController.text, this._confirmPasswordController.text) != null) {
      setState(() {
        _confirmPasswordError = FormValidator().validateConfimPassWord(this._newPasswordController.text, this._confirmPasswordController.text);
      });

      isValid = false;
    }

    
    return isValid;
  }
}
