import 'dart:io';

import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/services/socket.service.dart';
import 'package:booking_grab_apis/services/users.service.dart';
import 'package:booking_grab_apis/validators/form_validator.dart';
import 'package:booking_grab_apis/views/main_screen.dart';
import 'package:booking_grab_apis/views/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _loginScreen createState() => _loginScreen(); 
  
}

class _loginScreen extends State<LoginScreen> {
  TextEditingController _phoneNumberController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  String _phoneNumberError;
  String _passwordError;
  
  bool isLogingIn = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            color: myColors.whiteBackground,
            child: Column(
              children: [
                Text(
                  'Login',
                  style: TextStyle(
                    color:  myColors.title,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Roboto',
                    fontSize: 52,
                  ),
                  textAlign: TextAlign.center,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: myColors.mainColor,
                      width: 1.5
                    ),
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: myColors.whiteBackground,
                        ),
                        child: TextField(
                          style: TextStyle(
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.left,
                          enabled: !isLogingIn,
                          controller: _phoneNumberController,
                          onChanged: (value) {
                            _phoneNumberError = FormValidator().checkPhoneNumber(value);
                          },
                          decoration: InputDecoration(
                            labelText: 'Phone number',
                            errorText: _phoneNumberError,
                            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: myColors.mainColor, 
                                width: 1
                              ),
                              borderRadius:  BorderRadius.all(Radius.circular(6))
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20,),
                      Container(
                        decoration: BoxDecoration(
                          color: myColors.whiteBackground,
                        ),
                        child: TextField(
                          style: TextStyle(
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.left,
                          obscureText: true,
                          enabled: !isLogingIn,
                          controller: _passwordController,
                          onChanged: (value) {
                            _passwordError = FormValidator().checkPassword(value);
                          },
                          decoration: InputDecoration(
                            labelText: 'Password',
                            errorText: _passwordError,
                            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: myColors.mainColor, 
                                width: 1
                              ),
                              borderRadius:  BorderRadius.all(Radius.circular(6))
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        splashColor: Colors.grey,
                        padding: EdgeInsets.all(0),
                        color: isLogingIn ? Colors.grey : myColors.mainColor,
                        child: Container(
                          height: 65,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: isLogingIn ? CircularProgressIndicator() : Text('Login',
                              style: TextStyle(
                                color: myColors.whiteText,
                                fontSize: 20
                              ),
                            ),
                          ),
                        ),
                        onPressed: isLogingIn ? null : () {
                          setState(() {
                            isLogingIn = true;
                          });
            
                          _onLogin(context);
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                Center(
                  child: Text(
                    'Or',
                    style: TextStyle(
                      color: myColors.mainColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                const SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    splashColor: Colors.grey,
                    color: myColors.googleColor,
                    child: Container(
                      height: 65,
                      // width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text('Login with google',
                          style: TextStyle(
                            color: myColors.whiteText,
                            fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    onPressed: () => {

                    },
                  ),
                ),
                const SizedBox(height: 10,),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    splashColor: Colors.grey,
                    color: myColors.facebookColor,
                    child: Container(
                      height: 65,
                      // width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text('Login with facebook',
                          style: TextStyle(
                            color: myColors.whiteText,
                            fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    onPressed: () => {

                    },
                  ),
                ),
                const SizedBox(height: 30,),
                InkWell(
                  onTap: isLogingIn ? null : () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => SignupScreen()
                      )
                    );
                  },
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have not account? ',
                          style: TextStyle(
                            fontSize: 20
                          ),
                        ),
                        Text(
                          'Sign up',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: myColors.mainColor,
                            fontSize: 20
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 30),
              ],
            ),
          ),
        ],
      )
    );
  }

  _onLogin(context) async {
    try {
      bool isValid = _validateForm();

      if (!isValid) {
        setState(() {
          isLogingIn = false;
        });

        return;
      } 

      dynamic loginData = await UsersService().signIn(
        phoneNumber: _phoneNumberController.text,
        password: _passwordController.text,
      );

      String accessToken = loginData['accessToken'];
      String userID = loginData['userID'];

      if (accessToken.isNotEmpty) {
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.setString("token", accessToken);

        if (userID.isNotEmpty) {
          sharedPreferences.setString("userID", userID);
        }

        SocketService.socket.emit('connection', { 'token': accessToken});

        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
              builder: (context) => MainScreen(pageIndex: 0)
          )
        );

        return;
      }

      setState(() {
        isLogingIn = false;
      });

      return;
    } catch(error) {
      setState(() {
        isLogingIn = false;
      });
    }
  }

  bool _validateForm() {
    bool isValid = true;

    if (_phoneNumberController.text != null && FormValidator().checkPhoneNumber(_phoneNumberController.text) != null) {
      setState(() {
        _phoneNumberError = FormValidator().checkPhoneNumber(_phoneNumberController.text);
      });

      isValid = false;
    }


    if (_passwordController.text != null && FormValidator().checkPassword(_passwordController.text) != null) {
      setState(() {
        _passwordError = FormValidator().checkPassword(_passwordController.text);
      });

      isValid = false;
    }

    return isValid;
  }
  
}