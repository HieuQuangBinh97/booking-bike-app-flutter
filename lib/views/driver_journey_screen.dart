import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/socket.service.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DriverJourneyScreen extends StatefulWidget {
  VehicleBookings booking;
  bool isWaiting;
  
  DriverJourneyScreen({this.booking}) {
    MyGlobalVariable.isOnJourneyScreen = true;
  }


  @override
  _driverJourneyScreen createState() => _driverJourneyScreen(booking: booking);

}

class _driverJourneyScreen extends State<DriverJourneyScreen> {
  VehicleBookings booking;
  GoogleMapController mapController;
  PolylinePoints polylinePoints;
  Set<Polyline> setPolylines;
  List<LatLng> polylineCoordinates = [];
  Map<PolylineId, Polyline> polylines = {};
  LatLng _startLocation;
  LatLng _destination;

  bool stepOneFinished = false;
  bool stepTwoFinished = false;
  bool stepThreeFinished = false;
  bool handleStepOne = false;
  bool handleStepTwo = false;
  bool handleStepThree = false;

  bool driverAccepted = false;

  String status;
  Color statusColor;

  _driverJourneyScreen({
    this.booking,
  }) {
    _startLocation = new LatLng(booking.startingPoint.lat, booking.startingPoint.lng);
    _destination = new LatLng(booking.destination.lat, booking.destination.lng);
  }

  final TextEditingController _controller = new TextEditingController();
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  final Set<Marker> _markers = {};
  VehicleBookingsService bookingsService;
  @override
  void initState() {
    bookingsService= new VehicleBookingsService();
    _createPolylines(_startLocation, _destination);
    super.initState();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Hành trình',
          style: TextStyle(
            fontSize: 25
          ),
        ),
        backgroundColor: myColors.mainColor,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left,
            size: 40,
          ),
          onPressed: (){
            MyGlobalVariable.isOnJourneyScreen = false;
            Navigator.of(context).pop();
          }
        ),
        
      ),
      body: Container(
        child: Stack(
          children: [
            GoogleMap(
              polylines: setPolylines,
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: new LatLng(this.booking.startingPoint.lat, this.booking.startingPoint.lng),
                zoom: 15.0,
              ),
              markers: _markers,
              trafficEnabled: true,
              buildingsEnabled: false,
              indoorViewEnabled: true,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 100,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: myColors.whiteBackground,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 10,
                      spreadRadius: 2
                    )
                  ]
                ),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      width: 80,
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          if (!stepOneFinished && !handleStepOne) {
                            _onComing();
                          }
                        },
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: stepOneFinished ? myColors.mainColor : Colors.grey[400],
                                  width: 3
                                ),
                                shape: BoxShape.circle,
                                color: myColors.whiteBackground
                              ),
                              child: Center(
                                child: handleStepOne ? CircularProgressIndicator() : Text('1', style: TextStyle(
                                  color: stepOneFinished ? myColors.mainColor : Colors.grey[400],
                                  fontSize: 25
                                ),),
                              ),
                            ),
                            Text(
                              'Đón khách',
                              style: TextStyle(
                                color: stepOneFinished ? myColors.mainColor : Colors.grey[400],
                                fontSize: 15
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 50,
                      child: Container(
                        child: Divider(
                          color: stepOneFinished ? myColors.mainColor : Colors.grey[400],
                          height: 0,
                          thickness: 5,
                          indent: 3,
                          endIndent: 3,
                        ),
                      ),
                    ),
                    Container(
                      width: 80,
                      alignment: Alignment.center,
                      child:  InkWell(
                        onTap: () {
                          if (!stepTwoFinished && !handleStepTwo) {
                            _onStart();
                          }
                        },
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: stepTwoFinished ? myColors.mainColor : Colors.grey[400],
                                  width: 2
                                ),
                                shape: BoxShape.circle,
                                color: myColors.whiteBackground
                              ),
                              child: Center(
                                child: handleStepTwo ? CircularProgressIndicator() : Text('2', style: TextStyle(
                                  color: stepTwoFinished ? myColors.mainColor: Colors.grey[400],
                                  fontSize: 25
                                ),),
                              ),
                            ),
                            Text(
                              'Bắt đầu',
                              style: TextStyle(
                                color: stepTwoFinished ? myColors.mainColor : Colors.grey[400],
                                fontSize: 15
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 50,
                      child: Container(
                        child: Divider(
                          color: stepTwoFinished ? myColors.mainColor : Colors.grey[400],
                          height: 0,
                          thickness: 5,
                          indent: 3,
                          endIndent: 3,
                        ),
                      ),
                    ),
                    Container(
                      width: 80,
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          if (!stepThreeFinished && !handleStepThree) {
                            _onFinished();
                          }
                        },
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                                  width: 2
                                ),
                                shape: BoxShape.circle,
                                color: myColors.whiteBackground
                              ),
                              child: Center(
                                child: handleStepThree ? CircularProgressIndicator() : Text('3', style: TextStyle(
                                  color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                                  fontSize: 25
                                ),),
                              ),
                            ),
                            Text(
                              'Nhận tiền',
                              style: TextStyle(
                                color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                                fontSize: 15
                              ),
                            )
                          ],
                        ),
                      )
                    ),
                    SizedBox(width: 50,
                      child: Container(
                        child: Divider(
                          color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                          height: 0,
                          thickness: 5,
                          indent: 3,
                          endIndent: 3,
                        ),
                      ),
                    ),
                    Container(
                      width: 80,
                      alignment: Alignment.center,
                      child: InkWell(
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                                  width: 2
                                ),
                                shape: BoxShape.circle,
                                color: myColors.whiteBackground
                              ),
                              child: Center(
                                child: Text('4', style: TextStyle(
                                  color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                                  fontSize: 25
                                ),),
                              ),
                            ),
                            Text(
                              'Hoàn thành',
                              style: TextStyle(
                                color: stepThreeFinished ? myColors.mainColor : Colors.grey[400],
                                fontSize: 15
                              ),
                            )
                          ],
                        ),
                      )
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 130,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: myColors.whiteBackground,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 20,
                      color: Colors.grey,
                      spreadRadius: 1
                    )
                  ]
                ),
                child: Column(
                  children: [
                    Text(
                      '${((this.booking.distances).round() / 667).round()} Phút (${((this.booking.distances).round() / 1000) } km)',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: myColors.mainColor
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Giá:',
                          style: TextStyle(
                            fontSize: 18,
                            color: myColors.placeholderText
                          ),
                        ),
                        SizedBox(width: 10,),
                        Text(
                          _getPriceAsString(this.booking.price ),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: myColors.accentColor,
                            fontSize: 20
                          ),
                        ),
                        SizedBox(width: 10,),
                        Text(
                          '(VND)',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: myColors.placeholderText,
                            fontSize: 25
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          Icons.location_pin
                        ),
                        SizedBox(width: 10,),
                        Flexible(
                          child: Text(
                            this.booking.startAddress,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: myColors.placeholderText,
                              fontSize: 18,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          Icons.outlined_flag
                        ),
                        SizedBox(width: 10,),
                        Flexible(
                          child: Text(
                            this.booking.destinationAddress,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: myColors.placeholderText,
                              fontSize: 18,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )
    );
  }

  String _getPriceAsString(double price) {
    int temp = (price).round();
    
    String priceAsString = '';

    while(temp >= 1000) {
      temp = (temp / 1000).toInt();

      priceAsString = priceAsString + '.000';
    }

    priceAsString = '${temp}${priceAsString}';

    return priceAsString;
  }

  void _onAddMarker({ LatLng position, BitmapDescriptor icon , String id}) {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(id),
        position: position,
        icon: icon,
      ));
    });
  }


  // draw direction on map
  _createPolylines(LatLng start, LatLng destination) async {
    BitmapDescriptor icon = await _createMarkerImageFromAsset('posters/current_location_2.png', 100);

    _onAddMarker(
      position: new LatLng(this.booking.startingPoint.lat, this.booking.startingPoint.lng),
      icon: icon,
      id: '000002'
    );

    BitmapDescriptor icon2 = await _createMarkerImageFromAsset('posters/destination_2.png', 200);

    _onAddMarker(
      position: new LatLng(this.booking.destination.lat, this.booking.destination.lng),
      icon: icon2,
      id: '000001'
    );
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polylines
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyBWRWyXqjkR9bW0Hz3Ld0qYbSlj0wjiebI", // Google Maps API Key
      PointLatLng(booking.startingPoint.lat, booking.startingPoint.lng),
      PointLatLng(booking.destination.lat, booking.destination.lng),
      travelMode: TravelMode.transit,
    );

    // Adding the coordinates to the list
    if (result.points.isNotEmpty) {
      polylineCoordinates = new List<LatLng>();
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    Random random = new Random();

    PolylineId id = PolylineId('poly_${random.nextInt(30)}');

    Polyline polyline = Polyline(
      polylineId: id,
      color: myColors.mainColor,
      points: polylineCoordinates,
      width: 10,
    );

    polylines[id] = polyline;

    setState(() {
      setPolylines = Set<Polyline>.of(polylines.values);
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  Future <BitmapDescriptor> _createMarkerImageFromAsset(String iconPath, int width) async {
    Uint8List markerIcon = await getBytesFromAsset(iconPath, width);
    BitmapDescriptor bitmapImage = BitmapDescriptor.fromBytes(markerIcon);
    return bitmapImage;
  }

  void showDriverCancelDialog(context) {
    showDialog(
      context: context,
      builder: (context) {
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height - 200,
          margin: EdgeInsets.fromLTRB(20, 100, 20 , 200),
          decoration: BoxDecoration(
            color: myColors.whiteBackground,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Stack(
            children: [
              Center(
                child: Text(
                  'Tài xế đang bận, vui lòng chọn một tài xế khác',
                  style: TextStyle(
                    color: myColors.accentColor,
                    fontSize: 25,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width / 2 - 20,
                  padding: EdgeInsets.all(10),
                  height: 80,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color:  myColors.accentColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              )
            ],
          )
        );
      },
    );
  }

  _onComing() async {
    setState(() {
      this.handleStepOne = true;
    });

    bool succeed = await this.bookingsService.comeCustomerPosition(
      bookingID: this.booking.vehicleBookingID
    );

    if (succeed == null || !succeed) {
      ToastHelper().showTopShortToastError('Xãy ra lỗi');

      setState(() {
        this.handleStepOne = false;
      });
      return;
    }

    setState(() {
      this.stepOneFinished = true;
      this.handleStepTwo = false;
    });
  }

  _onStart() async {
    setState(() {
      this.handleStepTwo = true;
    });

    bool succeed = await this.bookingsService.startJourney(
      bookingID: this.booking.vehicleBookingID
    );

    if (succeed == null || !succeed) {
      ToastHelper().showTopShortToastError('Xãy ra lỗi');
      setState(() {
        this.handleStepTwo = false;
      });
      return;
    }

    setState(() {
      this.stepTwoFinished = true;
      this.handleStepTwo = false;
    });
  }

  _onFinished() async {
    setState(() {
      this.handleStepThree = true;
    });

    bool succeed = await this.bookingsService.finishedJourney(
      bookingID: this.booking.vehicleBookingID
    );

    if (succeed == null || !succeed) {
      ToastHelper().showTopShortToastError('Xãy ra lỗi');
      setState(() {
        this.handleStepThree = false;
      });
      return;
    }

    setState(() {
      this.stepThreeFinished = true;
      this.handleStepThree = false;
    });
  }

}