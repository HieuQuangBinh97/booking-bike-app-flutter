import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/socket.service.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JourneyScreen extends StatefulWidget {
  dynamic driverID;
  VehicleBookings booking;
  bool isWaiting;
  
  JourneyScreen({this.booking}) {
    MyGlobalVariable.isOnJourneyScreen = true;
  }


  @override
  _journeyScreen createState() => _journeyScreen(booking: booking);

}

class _journeyScreen extends State<JourneyScreen> {
  VehicleBookings booking;
  GoogleMapController mapController;
  PolylinePoints polylinePoints;
  Set<Polyline> setPolylines;
  List<LatLng> polylineCoordinates = [];
  Map<PolylineId, Polyline> polylines = {};
  LatLng _startLocation;
  LatLng _destination;

  bool driverAccepted = false;
  bool isCanceling = false;
  String status;
  Color statusColor;

  _journeyScreen({
    this.booking,
  }) {
    _startLocation = new LatLng(booking.startingPoint.lat, booking.startingPoint.lng);
    _destination = new LatLng(booking.destination.lat, booking.destination.lng);
  }

  final TextEditingController _controller = new TextEditingController();
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  final Set<Marker> _markers = {};
  
  @override
  void initState() {
    _onListenSocketEvent();
    _createPolylines(_startLocation, _destination);
    super.initState();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  void _onListenSocketEvent() {
    SocketService.socket.on('BOOKING_DRIVER_ACCEPTED', (data) {
      VehicleBookings updatedBooking = VehicleBookings.fromJson(data['data']);
      MyGlobalVariable.currentBooking = updatedBooking;

      setState(() {
        this.booking = updatedBooking;
      });

      getStatus(this.booking.status);
    });

    SocketService.socket.on('BOOKING_DRIVER_DENIED', (data) {
      VehicleBookings updatedBooking = VehicleBookings.fromJson(data['data']);
      MyGlobalVariable.currentBooking = updatedBooking;
      setState(() {
        this.booking = updatedBooking;
      });

      getStatus(this.booking.status);
      showDriverCancelDialog(context);
    });

    SocketService.socket.on('DRIVER_IS_COMING', (data) {
      VehicleBookings updatedBooking = VehicleBookings.fromJson(data['data']);
      MyGlobalVariable.currentBooking = updatedBooking;
      setState(() {
        this.booking = updatedBooking;
      });

      getStatus(this.booking.status);
    });

    SocketService.socket.on('BOOKING_START', (data) {
      VehicleBookings updatedBooking = VehicleBookings.fromJson(data['data']);
      MyGlobalVariable.currentBooking = updatedBooking;
      setState(() {
        this.booking = updatedBooking;
      });

      getStatus(this.booking.status);
    });

    SocketService.socket.on('BOOKING_FINISHED', (data) {
      VehicleBookings updatedBooking = VehicleBookings.fromJson(data['data']);
      MyGlobalVariable.currentBooking = updatedBooking;
      setState(() {
        this.booking = updatedBooking;
      });

      getStatus(this.booking.status);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Lộ trình',
          style: TextStyle(
            fontSize: 25
          ),
        ),
        backgroundColor: myColors.mainColor,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left,
            size: 40,
          ),
          onPressed: (){
            MyGlobalVariable.isOnJourneyScreen = false;
            Navigator.of(context).pop();
          }
        ),
        
      ),
      endDrawer: Drawer(
        child:  Container(
          child: Column(
            children: [
              Container(
                height: 100,
                color: myColors.mainColor,
              ),
              SizedBox(height: 5,),
              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                padding: EdgeInsets.all(0),
                splashColor: Colors.grey,
                color: myColors.accentColor,
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    color: myColors.accentColor,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[400],
                        blurRadius: 20,
                        spreadRadius: 2
                      )
                    ]
                  ),
                  child: Center(
                    child: isCanceling ? Row(
                      children: [
                        Text(
                        'Đang hủy hành trình',
                        style: TextStyle(
                          color: myColors.whiteText,
                          fontSize: 20
                        ),),
                        SizedBox(width: 5,),
                        CircularProgressIndicator()
                      ],
                    ) : Text(
                      'Hủy hành trình',
                      style: TextStyle(
                        color: myColors.whiteText,
                        fontSize: 20
                      ),
                    ),
                  ),
                ),
                onPressed: isCanceling ? null : () async  {
                  setState(() {
                    isCanceling = true;
                  });
                  dynamic res = await VehicleBookingsService().cancelBooking(bookingID: booking.vehicleBookingID);
                  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                  String userID = sharedPreferences.getString('userID');
                  sharedPreferences.remove('bookingID_'+userID);
                  MyGlobalVariable.currentBooking = null;
                  MyGlobalVariable.listDrivers = null;

                  if(res == null) {
                    setState(() {
                      isCanceling = false;
                    });
                    return;
                  }

                  MyGlobalVariable.currentBooking = null;
                  MyGlobalVariable.listDrivers = null;

                  setState(() {
                    setState(() {
                      isCanceling = false;
                    });
                    Navigator.of(context).pop();
                  });
                },
              )
            ],
          ),
        ),
      ),
      body: Container(
        child: Stack(
          children: [
            GoogleMap(
              polylines: setPolylines,
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: new LatLng(this.booking.startingPoint.lat, this.booking.startingPoint.lng),
                zoom: 15.0,
              ),
              markers: _markers,
              trafficEnabled: true,
              buildingsEnabled: false,
              indoorViewEnabled: true,
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 300,
                height: 60,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: myColors.whiteBackground,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 10,
                      spreadRadius: 2
                    )
                  ]
                ),
                child: Row(
                  children: [
                    Text('Trạng thái'),
                    SizedBox(width: 5,),
                    Container(
                      width: 200,
                      height: 60,
                      decoration: BoxDecoration(
                        color: statusColor,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Center(child: Text(
                        getStatus(this.booking.status),
                        style: TextStyle(color: myColors.whiteText, fontSize: 18),
                      ),)
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: 130,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: myColors.whiteBackground,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 20,
                      color: Colors.grey,
                      spreadRadius: 1
                    )
                  ]
                ),
                child: Column(
                  children: [
                    Text(
                      '${((this.booking.distances).round() / 667).round()} Phút (${((this.booking.distances).round() / 1000) } km)',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: myColors.mainColor
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Giá:',
                          style: TextStyle(
                            fontSize: 18,
                            color: myColors.placeholderText
                          ),
                        ),
                        SizedBox(width: 10,),
                        Text(
                          _getPriceAsString(this.booking.price ),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: myColors.accentColor,
                            fontSize: 20
                          ),
                        ),
                        SizedBox(width: 10,),
                        Text(
                          '(VND)',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: myColors.placeholderText,
                            fontSize: 25
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          Icons.location_pin
                        ),
                        SizedBox(width: 10,),
                        Flexible(
                          child: Text(
                            this.booking.startAddress,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: myColors.placeholderText,
                              fontSize: 18,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          Icons.outlined_flag
                        ),
                        SizedBox(width: 10,),
                        Flexible(
                          child: Text(
                            this.booking.destinationAddress,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: myColors.placeholderText,
                              fontSize: 18,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )
    );
  }

  String _getPriceAsString(double price) {
    int temp = (price).round();
    
    String priceAsString = '';

    while(temp >= 1000) {
      temp = (temp / 1000).toInt();

      priceAsString = priceAsString + '.000';
    }

    priceAsString = '${temp}${priceAsString}';

    return priceAsString;
  }

  void _onAddMarker({ LatLng position, BitmapDescriptor icon , String id}) {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(id),
        position: position,
        icon: icon,
      ));
    });
  }


  // draw direction on map
  _createPolylines(LatLng start, LatLng destination) async {
    BitmapDescriptor icon = await _createMarkerImageFromAsset('posters/current_location_2.png', 100);

    _onAddMarker(
      position: new LatLng(this.booking.startingPoint.lat, this.booking.startingPoint.lng),
      icon: icon,
      id: '000002'
    );

    BitmapDescriptor icon2 = await _createMarkerImageFromAsset('posters/destination_2.png', 200);

    _onAddMarker(
      position: new LatLng(this.booking.destination.lat, this.booking.destination.lng),
      icon: icon2,
      id: '000001'
    );
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polylines
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyBWRWyXqjkR9bW0Hz3Ld0qYbSlj0wjiebI", // Google Maps API Key
      PointLatLng(booking.startingPoint.lat, booking.startingPoint.lng),
      PointLatLng(booking.destination.lat, booking.destination.lng),
      travelMode: TravelMode.transit,
    );

    // Adding the coordinates to the list
    if (result.points.isNotEmpty) {
      polylineCoordinates = new List<LatLng>();
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    Random random = new Random();

    PolylineId id = PolylineId('poly_${random.nextInt(30)}');

    Polyline polyline = Polyline(
      polylineId: id,
      color: myColors.mainColor,
      points: polylineCoordinates,
      width: 10,
    );

    polylines[id] = polyline;

    setState(() {
      setPolylines = Set<Polyline>.of(polylines.values);
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  Future <BitmapDescriptor> _createMarkerImageFromAsset(String iconPath, int width) async {
    Uint8List markerIcon = await getBytesFromAsset(iconPath, width);
    BitmapDescriptor bitmapImage = BitmapDescriptor.fromBytes(markerIcon);
    return bitmapImage;
  }

  String getStatus(String bookingStatus) {
    Color color = Colors.blue;
    String status = 'Đang chờ tài xế';
    switch (bookingStatus) {
      case 'PROCESSING':
        color = Colors.blue;
        status = 'Đang chờ tài xế';
        break;
      case 'DRIVER_ACCEPTED':
        color = Colors.greenAccent;
        status =  'Tài xế đã chấp nhận';
        break;
      case 'DRIVER_IS_COMING':
        color = Colors.green[600];
        status = 'Tài xế đang tới';
        break;
      case 'DRIVER_DENIED':
        color = Colors.redAccent;
        status = 'Tài xế đã từ chối';
        break;
      case 'RUNNING':
        color = Colors.greenAccent;
        status = 'Khởi hành';
        break;
      case 'FINISHED':
        color = Colors.lime[800];
        status = 'Kết thúc hành trình';
        break;
      case 'CANCELED':
        color = Colors.redAccent;
        status = 'Hành trình bị hủy';
        break;
    }

    setState(() {
      this.statusColor = color;
    });

    return status;
  }

  void showDriverCancelDialog(context) {
    showDialog(
      context: context,
      builder: (context) {
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height - 200,
          margin: EdgeInsets.fromLTRB(20, 100, 20 , 200),
          decoration: BoxDecoration(
            color: myColors.whiteBackground,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Stack(
            children: [
              Center(
                child: Text(
                  'Tài xế đang bận, vui lòng chọn một tài xế khác',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: myColors.accentColor,
                    fontSize: 25,
                    
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width / 2 - 20,
                  padding: EdgeInsets.all(10),
                  height: 80,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color:  myColors.accentColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              )
            ],
          )
        );
      },
    );
  }

}