import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/main.dart';
import 'package:booking_grab_apis/models/notifications.dart';
import 'package:booking_grab_apis/services/notificationsService.dart';
import 'package:booking_grab_apis/views/split_items/notification_item.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _notificationScreen createState() => _notificationScreen();
  
}

class _notificationScreen extends State<NotificationScreen> with SingleTickerProviderStateMixin {
  List<Notifications> notifications;
  NotificationsService notificationsService = new NotificationsService();

  AnimationController animationControllerOne;
  Animation<Color> animationOne;
  Animation<Color> animationTwo;

  bool isEmpty = false;
  @override
  void initState() {
    loadData();
    super.initState();
    animationControllerOne = AnimationController(
        duration: Duration(milliseconds: 2000),
        vsync: this
    );
    animationOne = ColorTween(begin: Colors.grey,end: Colors.white70).animate(animationControllerOne);
    animationTwo = ColorTween(begin: Colors.white70,end: Colors.grey).animate(animationControllerOne);
    animationControllerOne.forward();
    animationControllerOne.addListener((){
      if(animationControllerOne.status == AnimationStatus.completed){
        animationControllerOne.reverse();
      } else if(animationControllerOne.status == AnimationStatus.dismissed){
        animationControllerOne.forward();
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    if (animationControllerOne != null) {
      animationControllerOne.dispose();
    }
    super.dispose();
    print('stop animation');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: notifications == null ? buildLoadingScreen() : Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            Row(
              children: [
                Text(
                  'Thông báo',
                  style: TextStyle(
                    color:  myColors.title,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Roboto',
                    fontSize: 40,
                  ),
                )
              ],
            ),
            const SizedBox(height: 10,),
            Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                'Mới',
                style: TextStyle(
                  fontSize: 20,
                ),
              )
            ),
            Column(
              children: buildNewNotification()
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                'Gần đây',
                style: TextStyle(
                  fontSize: 20,
                ),
              )
            ),
            Column(
              children: buildEarlyNotification()
            ),
          ],
        ),
      ),
    );
  }

  loadData() async {
    if (MyGlobalVariable.notifications.length > 0) {

      setState(() {
        this.notifications = MyGlobalVariable.notifications;
        this.animationControllerOne.stop();
      });

      return;
    }

    List<Notifications> listNotification = await this.notificationsService.getNotifications();

    if (listNotification == null) {
      setState(() {
        this.animationControllerOne.stop();
        this.isEmpty = true;
      });

      return;
    }

    MyGlobalVariable.notifications = listNotification;
    this.animationControllerOne.stop();

    setState(() {
      this.notifications = listNotification;
    });
  }

  buildLoadingScreen() {
    if (isEmpty) {
      return Container(
        child: Center(
          child: Text('Chưa có thông báo nào'),
        ),
      );
    }

    return Container(
      child: SafeArea(
        child: ShaderMask(
          shaderCallback: (rect){
            return LinearGradient(
              tileMode: TileMode.clamp,
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [animationOne.value,animationTwo.value]).createShader(rect,textDirection: TextDirection.ltr
            );
          },
          child: SizedBox(
            height: MediaQuery.of(context).size.height-50,
            child: ListView(
              children: [
                SizedBox(height: 10),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  decoration: BoxDecoration(
                    color: myColors.whiteBackground,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                SizedBox(height: 30,),
                Container(
                  width: 200,
                  height: 20,
                  color: myColors.whiteBackground,
                ),
                SizedBox(height: 10,),
                Column(
                  children: [
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: myColors.whiteBackground,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: myColors.whiteBackground,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: myColors.whiteBackground,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: myColors.whiteBackground,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: myColors.whiteBackground,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(height: 10,),
                  ],
                )
              ],
            )
          )
        ),
      )
    );
  }
  List<Widget> buildNewNotification() {
    List<Notifications> listNewNotifications = new List<Notifications>();
    if (this.notifications != null && this.notifications.length <= 3) {
      listNewNotifications = this.notifications;
    }
    if (this.notifications != null && this.notifications.length > 3) {
      listNewNotifications = this.notifications.sublist(0, 3);
    }

    List<Widget> listWidgetNotification = new List<Widget>();

    listNewNotifications.forEach((element) {
      listWidgetNotification.add(NotificationItem(title: element.title, content: element.data ));
    });

    return listWidgetNotification;
  }

  List<Widget> buildEarlyNotification() {
    List<Notifications> listEarlyNotifications = new List<Notifications>();

    if (this.notifications != null && this.notifications.length > 3) {
      listEarlyNotifications = this.notifications.sublist(3, this.notifications.length);
    }

    List<Widget> listWidgetNotification = new List<Widget>();

    if (listEarlyNotifications.length == 0) {
      return listWidgetNotification;
    }

    listEarlyNotifications.forEach((element) {
      listWidgetNotification.add(NotificationItem(title: element.title, content: element.data ));
    });

    return listWidgetNotification;
  }
}