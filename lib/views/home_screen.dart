import 'package:avatar_glow/avatar_glow.dart';
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:booking_grab_apis/views/booking_screen.dart';
import 'package:booking_grab_apis/views/journey_screen.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  List<Widget> listNearlyBookings;

  @override
  void initState() {
    buildListHistory();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: myColors.whiteBackground,
        ),
        child: ListView(
          children: [
            const SizedBox(height: 10,),
            Container(
              width: MediaQuery.of(context).size.width,
              child: CarouselSlider(
                options: CarouselOptions(
                  height: 160,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 1500),
                  autoPlayCurve: Curves.fastLinearToSlowEaseIn,
                ),
                items: ['slide1.jpg','slide2.jpg','slide3.jpg','slide4.jpg'].map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        margin: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          color: myColors.placeholderText,
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                            image: new AssetImage('posters/${i}'),
                            fit: BoxFit.cover,
                          ),
                        )
                      );
                    },
                  );
                }).toList(),
              )
            ),
            const SizedBox(height: 10,),
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              height: 200,
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: new AssetImage('posters/banner.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Align(
                alignment: Alignment.centerRight,
                child: Container(
                  width: 160,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Đặt xe nhanh chống, tiện lợi!',
                        style: TextStyle(
                          color: myColors.mainColor,
                          fontSize: 20
                        ),
                      ),
                      SizedBox(height: 20,),
                      MaterialButton(
                          shape: CircleBorder(
                                                      
                          ),
                          splashColor: Colors.grey,
                          color: myColors.whiteBackground,
                          padding: EdgeInsets.all(0),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              color: myColors.mainColor,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[400],
                                  blurRadius: 20,
                                  spreadRadius: 2
                                )
                              ],
                              shape: BoxShape.circle,
                            ),
                            child: Center(
                              child: Text(
                                'Go!',
                                style: TextStyle(
                                  color: myColors.whiteText,
                                  fontSize: 40
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (MyGlobalVariable.currentBooking != null &&
                                MyGlobalVariable.currentBooking.status != 'PENDING' &&
                                MyGlobalVariable.currentBooking.status != 'FINISHED' && 
                                MyGlobalVariable.currentBooking.status != 'DRIVER_DENIED'
                            ) {
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) => JourneyScreen(booking: MyGlobalVariable.currentBooking,))
                              ).then((value) {
                                buildListHistory();
                              });

                              return;
                            } else if (MyGlobalVariable.currentBooking != null && MyGlobalVariable.currentBooking.status == 'FINISHED') {
                              MyGlobalVariable.currentBooking = null;
                              MyGlobalVariable.listDrivers = null;
                            }

                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => BookingScreen())
                            ).then((value) {
                                buildListHistory();
                              });
                          },
                        ),
                    ],
                  ),
                ),
              )
            ),
            const SizedBox(height: 20,),
            Container(
              height: 40,
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Text(
                'Chuyến đi gần đây',
                style: TextStyle(
                  fontSize: 20
                ),
              ),
            ),
            const SizedBox(height: 5,),
            Column(
              children: this.listNearlyBookings == null ? [CircularProgressIndicator()] : this.listNearlyBookings
            )
          ],
        ),
      )
    );
  }

  buildListHistory() async {
    try {
      List<VehicleBookings> list = await VehicleBookingsService().findManyUserBooking();
      List<Widget> widget = new List<Widget>();

      if (list.length == 0) {
        setState(() {
          this.listNearlyBookings = [
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Colors.grey[200],
              ),
              child: Center( child: Text('chưa có chuyến đi nào gần đây')),
            )
          ];
        });
        return;
      }
      
      list.forEach((booking) { 
        dynamic status = getStatus(booking.status);
        widget.add(
          new Container(
            height: 160,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: myColors.whiteBackground,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  color: Colors.grey,
                  spreadRadius: 0,
                )
              ]
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Flexible(
                      child: Container(
                        width: 150,
                        height: 30,
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: status['statusColor']
                        ),
                        child: Center(child: Text(status['status'], style: TextStyle(color: myColors.whiteText),)),
                      )
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Icon(
                      Icons.location_pin
                    ),
                    SizedBox(width: 10,),
                    Flexible(
                      child: Text(
                        booking.startAddress,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: myColors.placeholderText,
                          fontSize: 18,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Icon(
                      Icons.outlined_flag
                    ),
                    SizedBox(width: 10,),
                    Flexible(
                      child: Text(
                        booking.destinationAddress,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: myColors.placeholderText,
                          fontSize: 18,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '${((booking.distances).round() / 667).round()} Phút (${((booking.distances).round() / 1000) } km)',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: myColors.mainColor
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Giá:',
                      style: TextStyle(
                        fontSize: 18,
                        color: myColors.placeholderText
                      ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      _getPriceAsString(booking.price ),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: myColors.accentColor,
                        fontSize: 20
                      ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      '(VND)',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: myColors.placeholderText,
                        fontSize: 25
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      });

      setState(() {
        this.listNearlyBookings = widget;
      });
    } catch(error) {
      setState(() {
        this.listNearlyBookings = [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: Colors.grey[200],
            ),
            child: Text('chưa có chuyến đi nào gần đây'),
          )
        ];
      });
      return;
    }
  }

  dynamic getStatus(String status) {
    Color statusColors = Colors.blueAccent;
    String statusAsString = '';
    switch(status) {
      case 'PENDING':
        statusAsString = 'Chưa chọn tài xế';
        break;
      case 'PROCESSING':
        statusAsString = 'Đang kết nối đến tài xế';
        statusColors = Colors.yellowAccent;
        break;
      case 'DRIVER_ACCEPTED':
        statusAsString = 'Tài xế đã chấp nhận';
        statusColors = Colors.greenAccent;
        break;
      case 'DRIVER_DENIED':
        statusAsString = 'Tài xế đã từ chối';
        statusColors = Colors.grey[600];
        break;
      case 'CANCELED':
        statusAsString = 'Đã hủy';
        statusColors = Colors.redAccent;
        break;
      case 'FINISHED':
        statusAsString = 'Hoàn thành';
        statusColors = Colors.cyanAccent;
        break;
    }

    return {
      'status': statusAsString,
      'statusColor': statusColors
    };
  }

  String _getPriceAsString(double price) {
    int temp = (price).round();
    
    String priceAsString = '';

    while(temp >= 1000) {
      temp = (temp / 1000).toInt();

      priceAsString = priceAsString + '.000';
    }

    priceAsString = '${temp}${priceAsString}';

    return priceAsString;
  }
}