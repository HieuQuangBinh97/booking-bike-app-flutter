import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/socket.service.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:booking_grab_apis/views/driver_screen.dart';
import 'package:booking_grab_apis/views/home_screen.dart';
import 'package:booking_grab_apis/views/login_screen.dart';
import 'package:booking_grab_apis/views/notification_screen.dart';
import 'package:booking_grab_apis/views/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';

class MainScreen extends StatefulWidget{
  int pageIndex=0;
  MainScreen({this.pageIndex});
  @override
  _mainScreen createState() => _mainScreen(pageIndex: pageIndex);
}

class _mainScreen extends State<MainScreen> with WidgetsBindingObserver {
  SharedPreferences sharedPreferences;
  AppLifecycleState lifecycleState;
  int pageIndex;
  _mainScreen({this.pageIndex});

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  @override
  void dispose() {
    SocketService.socket.on('disconnected', (data) => null);
    print('disconnected');
    super.dispose();
  }

  final pageList=[
    HomeScreen(),
    DriverScreen(),
    NotificationScreen(),
    ProfileScreen(),
  ];

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");

    if (token == null) {
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen()
        ), (
        Route<dynamic> route) => false
      );
    }

    if (!SocketService.socket.connected) {
      new SocketService(token);
    }

    if (MyGlobalVariable.currentBooking != null) {
      return;
    }

    String userID = sharedPreferences.getString('userID');
    String bookingID = sharedPreferences.getString('bookingID_'+userID);
    VehicleBookings booking = await VehicleBookingsService().findOneBooking(
      bookingID: bookingID
    );

    if (booking != null) {
      MyGlobalVariable.currentBooking = booking;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageList[pageIndex],
      bottomNavigationBar: Container(
        child: BottomNavigationBar(
          currentIndex: pageIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: myColors.whiteBackground,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  color: myColors.placeholderText,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.home,
                  color: myColors.mainColor,
                  size: 30,
                ),
                title: Text(
                  "Home",
                  style: TextStyle(
                    color: myColors.mainColor
                  ),
                ),

            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.two_wheeler,
                  color: myColors.placeholderText,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.two_wheeler,
                  color: myColors.mainColor,
                  size: 30,
                ),
                title: Text(
                  "Tài xế",
                  style: TextStyle(
                    color: myColors.mainColor
                  ),
                ),
                backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.notifications,
                  color: myColors.placeholderText,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.notifications,
                  color: myColors.mainColor,
                  size: 30,
                ),
                title: Text(
                  "Thông báo",
                  style: TextStyle(
                    color: myColors.mainColor
                  ),
                ),
                backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                  color: myColors.placeholderText,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.person,
                  color: myColors.mainColor,
                  size: 30,
                ),
                title: Text(
                  "Cá nhân",
                  style: TextStyle(
                    color: myColors.mainColor
                  ),
                ),
                backgroundColor: Colors.blue
            )
          ],
          onTap: (index){
            setState(() {
              pageIndex=index;
            });
          },
        ),
      )

    );
  }
}