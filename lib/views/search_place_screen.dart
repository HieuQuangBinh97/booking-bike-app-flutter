import 'dart:math';
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/views/booking_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

final homeScaffoldKey = GlobalKey<ScaffoldState>();
final searchScaffoldKey = GlobalKey<ScaffoldState>();

final customTheme = ThemeData(
  primarySwatch: Colors.blue,
  brightness: Brightness.dark,
  accentColor: Colors.redAccent,
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.00)),
    ),
    contentPadding: EdgeInsets.symmetric(
      vertical: 12.50,
      horizontal: 10.00,
    ),
  ),
);

class CustomSearchScaffold extends PlacesAutocompleteWidget {
  CustomSearchScaffold()
      : super(
          apiKey: "AIzaSyAh-ZLRzTMcNQV6Kty1EKYgR4j5xQmZSZI",
          language: "vn",
          components: [],
        );

  @override
  _CustomSearchScaffoldState createState() => _CustomSearchScaffoldState();
}

class _CustomSearchScaffoldState extends PlacesAutocompleteState {
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: 'AIzaSyAh-ZLRzTMcNQV6Kty1EKYgR4j5xQmZSZI');
  
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      leading: IconButton(
        icon: Icon(
          Icons.keyboard_arrow_left,
          color: myColors.mainColor,
          size: 40,
        ),
        onPressed: (){
          Navigator.of(context).pop();
        }
      ),
      backgroundColor: myColors.whiteBackground,
      title: AppBarPlacesAutoCompleteTextField(
        textStyle: TextStyle(
          fontSize: 20
        ),
        textDecoration: InputDecoration(
          hintText: 'Nhập điểm đến',
          suffixIcon: Icon(
            Icons.location_on_outlined,
            color: myColors.mainColor,
            size: 30,
          ),
        ),
      )
    );
    final body = PlacesAutocompleteResult(
      onTap: (p) async {
        print(p);
        PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
        double lat = detail.result.geometry.location.lat;
        double lng = detail.result.geometry.location.lng;

        LatLng position = new LatLng(lat, lng);
        BookingScreen.searchPlace = p.description;
        BookingScreen.searchLocation = position;
        Navigator.of(context).pop();
      },
      logo: Center(
        child: Text('Tìm kiếm điểm đến'),
      ),
    );
    return Scaffold(appBar: appBar, body: body);
  }

  @override
  void onResponseError(PlacesAutocompleteResponse response) {
    super.onResponseError(response);
    searchScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  @override
  void onResponse(PlacesAutocompleteResponse response) {
    super.onResponse(response);
    if (response != null && response.predictions.isNotEmpty) {
      searchScaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text("Got answer")),
      );
    }
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;

      scaffold.showSnackBar(
        SnackBar(content: Text("${p.description} - $lat/$lng")),
      );
    }
  }
}

  