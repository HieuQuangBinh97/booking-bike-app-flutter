import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapsService {
  PolylinePoints polylinePoints;
  Set<Polyline> setPolylines;
  List<LatLng> polylineCoordinates = [];
  Map<PolylineId, Polyline> polylines = {};
  LatLng _currentLocation;
  LatLng _destination;
  
}