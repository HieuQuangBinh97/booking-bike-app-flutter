import 'dart:convert';
import 'dart:io';
import 'package:booking_grab_apis/constants/apis_url.dart';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/models/drivers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:async/async.dart';

class DriversService {
  Future<Drivers> registerDriver({
    String identityCardNumber,
    File frontOfIdentityCard,
    File backOfIdentityCard
  }) async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      var multipartRequest = new http.MultipartRequest('POST', Uri.parse(DriversAPIs.REGISTER_DRIVER));
      
      multipartRequest.headers['Authorization'] = authorization;

      var frontImageStream = new http.ByteStream(DelegatingStream.typed(frontOfIdentityCard.openRead()));
      var frontImageLength = await frontOfIdentityCard.length();
      var frontImageMultipartFile = await http.MultipartFile('frontOfIdentityCard', frontImageStream, frontImageLength , filename: basename(frontOfIdentityCard.path));

      var backImageStream = new http.ByteStream(DelegatingStream.typed(backOfIdentityCard.openRead()));
      var backImageLength = await backOfIdentityCard.length();
      var backImageMultipartFile = await http.MultipartFile('backOfIdentityCard', backImageStream, backImageLength , filename: basename(backOfIdentityCard.path));

      multipartRequest.fields.addAll({
        'identityCardNumber': identityCardNumber,
      });

      multipartRequest.files.add(frontImageMultipartFile);
      multipartRequest.files.add(backImageMultipartFile);
      
      final responseStream = await multipartRequest.send();

      final response = await http.Response.fromStream(responseStream);

      Map<String, dynamic> responseBody = json.decode(response.body);
      if (response == null || response.statusCode != 201) {
        ToastHelper().showTopShortToastError('Đã xảy ra lỗi');

        return null;
      }

      ToastHelper().showTopShortToastSuccessfuly('Đã gửi yêu cầu!');

      return Drivers.fromJson(responseBody);
    } catch(error) {
      print(error);
      ToastHelper().showTopShortToastError('Đã xảy ra lỗi');
      return null;
    }
  }

  Future<dynamic> getDriverNearLocation({
    double distance,
    double lat,
    double lng,
    String limit,
    String offset,
  }) async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';

      Map<String, String> params = {
        'distance': distance.toString(),
        'lat': lat.toString(),
        'lng': lng.toString(),
        'limit': limit,
        'offset': offset,
      };

      String query = Uri(queryParameters: params).query;

      final client = new http.Client();
      final response = await client.get(
        '${DriversAPIs.FIND_DRIVERS_NEAR_LOCATION}?'+ query,
        headers: {'Authorization': authorization},
      );

      if (response == null || response.statusCode != 200) {
        return null;
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);
      final drivers = mapResponse["list"];

      return drivers;
    } catch(error) {


      return null;
    }
  }

  Future<bool> rating({ int rating, String driverID }) async {
    try {
      Map data = {
        'rating': rating
      };

      dynamic body = json.encode(data);

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';

      final client = new http.Client();
      final response = await client.put(
        DriversAPIs.BASE_URL + '/' + driverID + '/rate',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
        body: body
      );

      if (response == null || response.statusCode != 200) {
        return false;
      }

      return true;
    } catch(error) {
      return false;
    }
  }

  Future<dynamic> getDriver() async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';

      final client = new http.Client();
      final response = await client.get(
        DriversAPIs.BASE_URL + '/me/statistic',
        headers: {'Authorization': authorization},
      );

      if (response == null || response.statusCode != 200) {
        if (response != null && response.statusCode == 404) {
          return {
            'code': '404'
          };
        }

        return null;
      }

      final responseBody= json.decode(response.body);

      return responseBody;
    } catch(error) {
      return null;
    }
  }
}