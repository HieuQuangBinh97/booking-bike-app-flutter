import 'dart:convert';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/models/drivers.dart';
import 'package:booking_grab_apis/models/position.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:booking_grab_apis/constants/apis_url.dart';
import 'package:http/http.dart' as http;

class VehicleBookingsService {
  Future<VehicleBookings> findOneBooking({
    String bookingID
  }) async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.get(
        VehicleBookingAPIs.BASE_URL+ '/' + bookingID,
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
      );

      if (response == null || response.statusCode != 200) {
        throw Exception('Failed to Booking vehicle'); 
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);

      VehicleBookings booking = VehicleBookings.fromJson(mapResponse);

      return booking;
    } catch(error) {
      return null;
    }
  }

   Future<List<VehicleBookings>> findManyUserBooking({
    String bookingID
  }) async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.get(
        VehicleBookingAPIs.BASE_URL,
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
      );

      if (response == null || response.statusCode != 200) {
        throw Exception('Failed to Booking vehicle'); 
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);

      List<VehicleBookings> booking = new List<VehicleBookings>();

      if (mapResponse['list'] != null && mapResponse['list'].length > 0) {
        mapResponse['list'].forEach((each) {
          booking.add(VehicleBookings.fromJson(each));
        });
      }
      

      return booking;
    } catch(error) {
      return null;
    }
  }

  Future<dynamic> createBooking({
    MyPosition startingPoint,
    MyPosition destination,
    double price,
    double distances,
    String startAddress,
    String destinationAddress,
  }) async {
    try {
      Map data = {
        'startingPoint': {
          'lat': startingPoint.lat,
          'lng': startingPoint.lng,
        },
        'destination':  {
          'lat': destination.lat,
          'lng': destination.lng,
        },
        'price': price,
        'distances': distances,
        'startAddress': startAddress,
        'destinationAddress': destinationAddress
      };

      dynamic body = json.encode(data);
      
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.post(
        VehicleBookingAPIs.CREATE_BOOKING,
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
        body: body
      );

      if (response == null || response.statusCode != 201) {
        ToastHelper().showTopShortToastError('An error occurred booking vehicle');
        throw Exception('Failed to Booking vehicle'); 
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);

      VehicleBookings newBooking = VehicleBookings.fromJson(mapResponse['vehicleBooking']);
      dynamic drivers = mapResponse['driverNearLocations'];

      return {
        'vehicleBooking': newBooking,
        'drivers': drivers
      };
    } catch(error) {
      return null;
    }
  }

  Future<VehicleBookings> selectDriver({
    String bookingID,
    String driverID
  }) async {
    try {
      Map data = {
        'driverID': driverID.toString() 
      };

      dynamic body = json.encode(data);

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.put(
        VehicleBookingAPIs.BASE_URL + '/' + bookingID + '/selectDriver',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
        body: body
      );

      if (response == null || response.statusCode != 200) {
        ToastHelper().showTopShortToastError('Failed to select driver');
        throw Exception('Failed to select driver'); 
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);
      
      return VehicleBookings.fromJson(mapResponse);
    } catch(error) {
      return null;
    }
  }

  Future<VehicleBookings> driverConfirm({
    String bookingID,
    String status
  }) async {
    try {
      Map data = {
        'status': status.toString() 
      };

      dynamic body = json.encode(data);

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.put(
        VehicleBookingAPIs.BASE_URL + '/' + bookingID + '/driver/confirm',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
        body: body
      );

      if (response == null || response.statusCode != 200) {
        ToastHelper().showTopShortToastError('Failed to confirm');
        throw Exception('Failed to confirm'); 
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);
      
      return VehicleBookings.fromJson(mapResponse);
    } catch(error) {
      return null;
    }
  }

  Future<bool> startJourney({
    String bookingID,
  }) async {
    try {

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.put(
        VehicleBookingAPIs.BASE_URL + '/' + bookingID + '/start',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
      );

      if (response == null || response.statusCode != 200) {
        ToastHelper().showTopShortToastError('Failed to confirm');
        throw Exception('Failed to confirm');
      }

      bool isStarted = json.decode(response.body);
      
      return isStarted;
    } catch(error) {
      return null;
    }
  }

  Future<bool> comeCustomerPosition({
    String bookingID,
  }) async {
    try {

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.put(
        VehicleBookingAPIs.BASE_URL + '/' + bookingID + '/driverStart',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
      );

      if (response == null || response.statusCode != 200) {
        throw Exception('Failed to confirm'); 
      }

      bool isStarted = json.decode(response.body);
      
      return isStarted;
    } catch(error) {
      return null;
    }
  }



  Future<bool> finishedJourney({
    String bookingID,
  }) async {
    try {

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.put(
        VehicleBookingAPIs.BASE_URL + '/' + bookingID + '/finished',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
      );

      if (response == null || response.statusCode != 200) {
        throw Exception('Failed to confirm'); 
      }

      bool isFinished = json.decode(response.body);
      
      return isFinished;
    } catch(error) {
      return null;
    }
  }

  Future<bool> cancelBooking({
    String bookingID,
  }) async {
    try {

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();

      final response = await client.put(
        VehicleBookingAPIs.BASE_URL + '/' + bookingID + '/cancel',
        headers: {
          'Authorization': authorization,
          'Content-Type': 'application/json'
        },
      );

      if (response == null || response.statusCode != 200) {
        throw Exception('Failed to confirm'); 
      }

      bool isCancel = json.decode(response.body);
      
      return isCancel;
    } catch(error) {
      return null;
    }
  }
}