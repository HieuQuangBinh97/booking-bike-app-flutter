import 'package:booking_grab_apis/constants/apis_url.dart';
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/helpers/dialogHelper.dart';
import 'package:booking_grab_apis/helpers/notificationHelper.dart';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/main.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:booking_grab_apis/services/vehicleBookings.service.dart';
import 'package:booking_grab_apis/views/booking_screen.dart';
import 'package:booking_grab_apis/views/driver_journey_screen.dart';
import 'package:booking_grab_apis/views/journey_screen.dart';
import 'package:booking_grab_apis/views/split_items/rating_screen.dart';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class NotificationTypes {
  static String BOOKING_SELECT_DRIVER = 'BOOKING_SELECT_DRIVER';
  static String BOOKING_DRIVER_ACCEPTED = 'BOOKING_DRIVER_ACCEPTED';
  static String BOOKING_DRIVER_DINED = 'BOOKING_DRIVER_DINED';
  static String ADMIN_ACCEPTED_DRIVER = 'ADMIN_ACCEPTED_DRIVER';
  static String ADMIN_DENIED_DRIVER = 'ADMIN_DENIED_DRIVER';
  static String RATING_DRIVER = 'RATING_DRIVER';
  static String DRIVER_IS_COMING = 'DRIVER_IS_COMING';
  static String BOOKING_CANCELED = 'BOOKING_CANCELED';
}

class SocketService {
  static IO.Socket socket;
  NotificationHelper notificationHelper = new NotificationHelper();

  SocketService(String token) {
    init(token);
    listenEvents();
  }

  listenEvents() {
    socket.on('disconnected', (data) {
      notificationHelper.addNotification(data);

      ToastHelper().showTopShortToastError('socket was disconnected');
    });

    socket.on('BOOKING_DRIVER_ACCEPTED', (data) {
      String notificationID = data['notification']['notificationID'];

      if (notificationID != MyGlobalVariable.lastNotificationID) {
        notificationHelper.addNotification(data);

        if (MyGlobalVariable.isOnJourneyScreen == false) {
          handleDriverAcceptedBooking(data);
        }
      }
    });

    socket.on('BOOKING_START', (data) {
      String notificationID = data['notification']['notificationID'];

      if (notificationID != MyGlobalVariable.lastNotificationID) {
        notificationHelper.addNotification(data);

        if (MyGlobalVariable.isOnJourneyScreen == false) {
          handleStartBooking(data);
        }
      }
    });

    socket.on('BOOKING_FINISHED', (data) {
      notificationHelper.addNotification(data);

      handlefinishedBooking(data);
    });

    socket.on('BOOKING_DRIVER_DENIED', (data) {
      String notificationID = data['notification']['notificationID'];

      if (notificationID != MyGlobalVariable.lastNotificationID) {
        notificationHelper.addNotification(data);

        if (MyGlobalVariable.isOnJourneyScreen == false) {
          handleDriverDeniedBooking(data);
        }
      }
    });


    socket.on(NotificationTypes.BOOKING_SELECT_DRIVER, (data) {
      String notificationID = data['notification']['notificationID'];

      if (notificationID != MyGlobalVariable.lastNotificationID) {
        notificationHelper.addNotification(data);

        handleCustomerSelectDriver(data);
      }
    });

    socket.on(NotificationTypes.BOOKING_CANCELED, (data) {
      notificationHelper.addNotification(data);

      if (MyGlobalVariable.isOnJourneyScreen == false) {
        handleCanceledBooking();
      }
    });

    socket.on(NotificationTypes.DRIVER_IS_COMING, (data) {
      notificationHelper.addNotification(data);
    
      if (!MyGlobalVariable.isOnJourneyScreen) {
        handleDriverIsComing(data);
      }
    });

    socket.on(NotificationTypes.ADMIN_ACCEPTED_DRIVER, (data) {
      notificationHelper.addNotification(data);
    });

    socket.on(NotificationTypes.ADMIN_DENIED_DRIVER, (data) {
      notificationHelper.addNotification(data);
    });
  }

  IO.Socket getSocketClient() {
    return socket;
  }

  static init(String token) {
    socket = IO.io(HOST, <String, dynamic>{
      'transports': ['websocket'],
      'extraHeaders': {'token': token}
    });
  }

  handleCustomerChooseDriver(data) async {
    try {
      showDialog(
        context: navigatorKey.currentContext,
        builder: (BuildContext context) {
          return Container(
            width:  300,
            height: 300,
            color: myColors.mainColor,
          );
        }
      );
    } catch(error) {

    }
  }

  handleDriverAcceptedBooking(data) {
    try {
      VehicleBookings booking = VehicleBookings.fromJson(data["data"]);
      MyGlobalVariable.currentBooking = booking;

      BuildContext context = navigatorKey.currentContext;

      List<MyActions> actions = new List<MyActions> ();

      actions.add(
        new MyActions(
          color: myColors.mainColor,
          name: 'Xem hành trình',
          handle: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => JourneyScreen(booking: booking,)
              )
            );
          },
        )
      );

      DialogHelper().showNotification('Tài xế đã chấp nhận yêu cầu của bạn', actions);
    } catch (error) {
      print(error);
    }
  }


  handleStartBooking(data) {
    try {
      VehicleBookings booking = VehicleBookings.fromJson(data["data"]);
      MyGlobalVariable.currentBooking = booking;

      BuildContext context = navigatorKey.currentContext;

      List<MyActions> actions = new List<MyActions> ();

      actions.add(
        new MyActions(
          color: myColors.mainColor,
          name: 'Xem hành trình',
          handle: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => JourneyScreen(booking: booking,)
              )
            );
          },
        )
      );

      DialogHelper().showNotification('Chuyến đi của bạn đã bắt đầu', actions);
    } catch (error) {
      print(error);
    }
  }

  handlefinishedBooking(data) {
    try {
      VehicleBookings booking = VehicleBookings.fromJson(data["data"]);
      MyGlobalVariable.currentBooking = booking;
      MyGlobalVariable.listDrivers = null;

      Navigator.of(navigatorKey.currentContext).push(
        MaterialPageRoute(
          builder: (context) => RatingScreen(driverID: booking.driverID,)
        )
      );
      
    } catch (error) {
      print(error);
    }
  }

  handleDriverDeniedBooking(data) {
    try {
      VehicleBookings booking = VehicleBookings.fromJson(data["data"]);
      MyGlobalVariable.currentBooking = booking;

      BuildContext context = navigatorKey.currentContext;

      List<MyActions> actions = new List<MyActions> ();

      actions.add(
        new MyActions(
          color: myColors.mainColor,
          name: 'Tiếp tục',
          handle: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => BookingScreen()
              )
            );
          }
        )
      );

      DialogHelper().showNotification('Tài xế đã từ chối yêu cầu', actions);
    } catch(error) {

    }
  }

  handleCustomerSelectDriver(data) {
    try {
      VehicleBookings booking = VehicleBookings.fromJson(data["data"]);
      MyGlobalVariable.currentBooking = booking;

      BuildContext context = navigatorKey.currentContext;

      List<MyActions> actions = new List<MyActions> ();

      actions.add(
        new MyActions(
          color: myColors.accentColor,
          name: 'Từ chối',
          handle: () {
            VehicleBookingsService().driverConfirm(
              bookingID: booking.driverID,
              status: 'denied',
            );

            Navigator.of(context).pop();
          },
        )
      );

      actions.add(
        new MyActions(
          color: myColors.mainColor,
          name: 'Chấp nhận',
          handle: () async {
            VehicleBookings acceptedBooking = await VehicleBookingsService().driverConfirm(
              bookingID: booking.vehicleBookingID,
              status: 'accepted'
            );

            if(acceptedBooking == null) {
              return;
            }

            Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                builder: (context) => DriverJourneyScreen(booking: acceptedBooking,)
              )
            );
          },
        )
      );

      DialogHelper().showNotification('${booking.customerData.lastName } đã chọn bạn làm tài xế', actions);
    } catch (error) {
      print(error);
    }
  }

  handleCanceledBooking() {
    showDialog(
      context: navigatorKey.currentContext,
      builder: (BuildContext context){
        return Container(
          child: Card(
            child: InkWell(
              child: Container(
                height: 200,
                width: MediaQuery.of(context).size.width,
                color: myColors.whiteBackground,
                child: Text('Hành trình đã bị hủy bỏ'),
              ),
              onTap: () {
              },
            ),
          )
        );
      }
    );
  }

  handleDriverIsComing(data) {
    try {
      VehicleBookings booking = VehicleBookings.fromJson(data["data"]);

      showDialog(
        context: navigatorKey.currentContext,
        builder: (BuildContext context){
          return Container(
            height: 260,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.fromLTRB(10, 200, 10, 200),
            
            child: Card(
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Stack(
                  children: [
                    Container(
                      height: 260,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage('posters/driverAcceptedBanner.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Center(
                        child: Text(
                          '${booking.customerData.lastName} Đã hủy chuyến',
                          style: TextStyle(
                            color: Colors.purpleAccent,
                            fontSize: 35,
                            fontWeight: FontWeight.bold,
                            shadows: [
                              Shadow(
                                color: Colors.grey,
                                blurRadius: 10,
                              )
                            ]
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                          ),
                          padding: EdgeInsets.all(0),
                          child: Container(
                            width: 200,
                            height: 60,
                            decoration: BoxDecoration(
                              color: myColors.mainColor,
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 10,
                                  spreadRadius: 2,
                                )
                              ],
                            ),
                            child: Center(
                              child: Text(
                                'OK',
                                style: TextStyle(
                                  color: myColors.whiteText,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                          ),
                          onPressed:() {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          },
                        )
                      ),
                    ),
                  ],
                )
              ),
            )
          );
        }
      );
    } catch (error) {
      print(error);
    }
  }
}