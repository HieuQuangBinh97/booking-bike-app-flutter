import 'dart:convert';
import 'package:booking_grab_apis/constants/apis_url.dart';
import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/helpers/toastHelper.dart';
import 'package:booking_grab_apis/models/users.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UsersService {
  void showTopShortToastError(String message) {
    Fluttertoast.showToast(
        msg: message,
        textColor: myColors.whiteText,
        backgroundColor: myColors.accentColor,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  void showTopShortToastSucces(String message) {
    Fluttertoast.showToast(
        msg: message,
        textColor: myColors.whiteText,
        backgroundColor: myColors.mainColor,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  Future<dynamic> signUp({ 
    String firstName,
    String lastName,
    String email,
    String phoneNumber,
    String password,
    String confirmPassword,
    String avatar
  }) async {
    try {
      Map data = {
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'phoneNumber': phoneNumber,
        'password': password,
        'confirmPassword': confirmPassword,
        'avatar': avatar != null ? avatar : '',
      };

      final client = new http.Client();
      final response = await client.post(
        UsersAPIs.SIGN_UP,
        body: data
      );
      final responseBody= json.decode(response.body);

      if (response == null || response.statusCode != 201) {
        String message = responseBody['message'];
        showTopShortToastError(message);

        return null;
      }

      showTopShortToastSucces('signup successfuly!');

      return responseBody;
    } catch(error) {
      print(error);
      showTopShortToastError('An error occurred signUp');
    }
  }

  Future<dynamic> signIn({
    String phoneNumber,
    String password
  }) async {
    try {
      Map<String, dynamic> data = {
        'phoneNumber': phoneNumber,
        'password': password
      };

      final client = new http.Client();
      final response = await client.post(UsersAPIs.SIGN_IN, body: data);
      final responseBody= json.decode(response.body);

      if (response == null || response.statusCode != 201) {
        String message = responseBody['name'];
        showTopShortToastError(message); 
        return null;
      }

      showTopShortToastSucces('signIn successfuly!');

      return responseBody;
    } catch(error) {
      showTopShortToastError('An error occurred signIn');
      // handle when have error
    }
  }

  Future<Users> getProfile() async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();
      final response = await client.get(
        UsersAPIs.GET_PROFILE,
        headers: {'Authorization': authorization});

      if (response == null || response.statusCode != 200) {
        print(response.body);
        throw Exception('Failed to get profile'); 
      }

      Map<String, dynamic> mapResponse = json.decode(response.body);

      return Users.fromJson(mapResponse);
    } catch(error) {
      ToastHelper().showTopShortToastError('Failed to get profile');
    }
  }

  Future<bool> updateProfile(Map<String, dynamic> data) async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();
      final response = await client.put(
        UsersAPIs.UPDATE_PROFILE,
        headers: {'Authorization': authorization},
        body: data
      );

      if (response == null || response.statusCode != 200) {
        print(response.body);
        throw Exception('Failed to update profile'); 
      }

      return true;
    } catch(error) {
      showTopShortToastError('Failed to update profile');
    }
  }

  Future<bool> changePassword({
    String oldPassword,
    String newPassword,
    String confirmPassword
  }) async {
    try {
      Map<String, dynamic> data = {
        'oldPassword': oldPassword,
        'newPassword': newPassword,
        'confirmPassword': confirmPassword
      };

      
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';
      final client = new http.Client();
      final response = await client.put(
        UsersAPIs.CHANGE_PASSWORD,
        headers: {'Authorization': authorization},
        body: data
      );

      if (response == null || response.statusCode != 200) {
        print(response.body);
        throw Exception('Failed to change password'); 
      }

      return true;
    } catch(error) {
      return false;
    }
  }
}