import 'package:booking_grab_apis/models/notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:booking_grab_apis/constants/apis_url.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class NotificationsService {
  Future<List<Notifications>> getNotifications() async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String token = sharedPreferences.getString('token');
      String authorization = 'Bearer ${token}';

      final client = new http.Client();
      final response = await client.get(
        NotificationsAPIs.GET_NOTIFICATION,
        headers: {'Authorization': authorization},
      );

      if (response == null || response.statusCode != 200) {
        return null;
      }

      final responseBody= json.decode(response.body);

      List<Notifications> listNotification = new List<Notifications>();

      if (responseBody != null && responseBody['list'] != null && responseBody['list'].length > 0) {
        responseBody['list'].forEach((each) {
          listNotification.add(Notifications.fromJson(each));
        });
      }

      return listNotification;
    } catch(error) {
      return null;
    }
  }
}