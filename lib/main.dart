import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/views/welcome_screen.dart';
import 'package:flutter/material.dart';

final navigatorKey = GlobalKey<NavigatorState>();
void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        backgroundColor: myColors.whiteBackground
      ),
      
      home: WelcomeScreen(),
      navigatorKey: navigatorKey,
    );
  }
}