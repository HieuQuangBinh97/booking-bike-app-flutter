import 'package:email_validator/email_validator.dart';

class FormValidator {
  String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);

    if (value.isEmpty) {
      return 'Please enter password';
    }

    if (!regex.hasMatch(value)) {
      return 'Enter valid password';
    }

    return null;
  }

  String validatePhoneNumber(String value) {
    Pattern pattern = r'^[0-9\-\+]{9,15}$';

    RegExp regex = new RegExp(pattern);

    if (value.isEmpty) {
      return 'Please type phoneNumber';
    }

    if (!regex.hasMatch(value)) {
      return 'Invalid phoneNumber fomat';
    }

    return null;
  }

  String checkPassword(String value) {
    if (value.isEmpty) {
      return 'Please enter password';
    }

    return null;
  }

  String checkFirstName(String value){
    if(value.isEmpty){
      return "Please enter firstName";
    }

    return null;
  }

  String checkLastName(String value){
    if(value.isEmpty){
      return "Please enter lastName";
    }

    return null;
  }


  String checkConfirmPassword(String value){
    if(value.isEmpty){
      return "Please enter ConfirmPassword";
    }

    return null;
  }

  String checkEmail(String value) {
    if (value.isEmpty) {
      return 'Please enter email';
    } 

    if (!EmailValidator.validate(value)) {
      return 'Invalid Email';
    }

    return null;
  }

  String checkPhoneNumber(String value) {
    if (value.isEmpty) {
      return 'Please enter phoneNumber';
    }

    return validatePhoneNumber(value);
  }


  String validateConfimPassWord(String password, String confirmPassword) {
    if (confirmPassword.isEmpty) {
      return 'Please enter confirm password';
    }

    if (password != confirmPassword) {
      return 'Incorrect cofirm password';
    }

    return null;
  }
}
