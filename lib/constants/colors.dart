import 'package:flutter/material.dart';

class MyColors {
  Color whiteBackground = Colors.white;
  Color title = Color.fromRGBO(44, 184, 244, 1);
  Color placeholderText = Colors.grey;
  Color whiteText = Colors.white;
  Color mainColor = Color.fromRGBO(44, 184, 244, 1);
  Color accentColor = new Color.fromRGBO(210, 114, 114, 1);
  Color facebookColor = Colors.indigoAccent[700];
  Color googleColor = Colors.red[600];
}

MyColors myColors = new MyColors();