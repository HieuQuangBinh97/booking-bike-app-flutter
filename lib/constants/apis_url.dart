// String HOST = 'https://booking-grabbike.azurewebsites.net';
String HOST = 'http://192.168.1.167:3000';

class UsersAPIs {
  static String SIGN_IN = HOST + '/auth/login/password';
  static String SIGN_UP = HOST + '/auth/signUp';
  static String GET_PROFILE = HOST + '/users/me';
  static String UPDATE_PROFILE = HOST + '/users/me';
  static String CHANGE_PASSWORD = HOST + '/users/me/password';
  static String CHANGE_CONTACT = HOST + '/users/me/contact';
  static String GET_USERS = HOST + '/users';
}


class DriversAPIs {
  static String REGISTER_DRIVER = HOST + '/drivers';
  static String GET_DRIVERS = HOST + '/drivers';
  static String FIND_DRIVERS_NEAR_LOCATION = HOST + '/drivers/nearLocation';
  static String BASE_URL = HOST + '/drivers';
}

class VehicleBookingAPIs {
  static String CREATE_BOOKING = HOST + '/vehicleBookings';
  static String BASE_URL = HOST + '/vehicleBookings';
}

class NotificationsAPIs {
  static String GET_NOTIFICATION = HOST + '/users/me/notifications';
}
