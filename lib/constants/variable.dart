import 'package:booking_grab_apis/models/notifications.dart';
import 'package:booking_grab_apis/models/vehicle_bookings.dart';
import 'package:flutter/material.dart';
import 'package:booking_grab_apis/models/users.dart';

class MyGlobalVariable {
  static bool isOnJourneyScreen = false;
  static VehicleBookings currentBooking = null;
  static BuildContext currentContext;
  static Users currentUser;
  static dynamic listDrivers;
  static List<Notifications> notifications = new List<Notifications> ();
  static String lastNotificationID = null;
}