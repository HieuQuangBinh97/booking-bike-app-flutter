class MyPosition {
  double lat;
  double lng;

  MyPosition({ this.lat, this.lng });

  factory MyPosition.fromJson(Map<String, dynamic> json) {
    MyPosition position = new MyPosition(lat: double.parse(json['lat'].toString()), lng: double.parse(json['lng'].toString()));

    return position;
  }
}