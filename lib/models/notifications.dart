import 'package:booking_grab_apis/models/users.dart';

class Notifications {
  String notificationID;
  String vehicleBookingID;
  String data;
  String title;
  String type;
  bool isRead;
  String createdBy;
  Users createdByData;
  String image;

  Notifications({
    this.notificationID,
    this.vehicleBookingID,
    this.data,
    this.title,
    this.type,
    this.isRead,
    // this.createdBy,
    // this.image,
  });

  factory Notifications.fromJson(json) {
    return new Notifications(
      notificationID: json['notificationID'],
      vehicleBookingID: json['vehicleBookingID'],
      data: json['data'],
      title: json['title'],
      type: json['type'],
      isRead: false,
      // createdBy: json['createdBy']['userID'],
      // image: json['createdBy']['avatar'],
    );
  }
}