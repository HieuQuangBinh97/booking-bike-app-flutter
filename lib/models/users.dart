class Users {
  String userID;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  String address;
  String username;
  String avatar;
  int identityCardNumber;
  String frontOfIdentityCard;
  String backOfIdentityCard;
  String status;
  String facebookID;
  String googleID;

  Users({
    this.userID,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
    this.address,
    this.username,
    this.avatar,
    this.identityCardNumber,
    this.frontOfIdentityCard,
    this.backOfIdentityCard,
    this.status,
    this.facebookID,
    this.googleID,
  });

  factory Users.fromJson(Map<String, dynamic> json) {
    Users user = Users(
        userID: json['userID'],
        firstName: json['firstName'],
        lastName: json['lastName'],
        email: json['email'],
        phoneNumber: json['phoneNumber'],
        address: json['address'],
        username: json['username'],
        avatar: json['avatar'],
        identityCardNumber: json['identityCardNumber'],
        frontOfIdentityCard: json['frontOfIdentityCard'],
        backOfIdentityCard: json['backOfIdentityCard'],
        status: json['status'],
        facebookID: json['facebookID'],
        googleID: json['googleID'],
    );
    return user;
  }
}