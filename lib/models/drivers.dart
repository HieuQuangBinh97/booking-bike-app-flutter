import 'package:booking_grab_apis/models/users.dart';

class Drivers {
  String driverID;
  String userID;
  String status;
  double lastLat;
  double lastLng;
  String lastLoggedAt;
  String createdBy;
  String createdAt;
  String updatedAt;
  double rating;
  int rateTimes;
  Users userData;

  Drivers({
    this.driverID,
    this.userID,
    this.status,
    this.lastLat,
    this.lastLng,
    this.createdAt,
    this.rating,
    this.rateTimes,
    this.userData,
  });

  factory Drivers.fromJson(Map<String, dynamic> json) {
    Drivers driver = new Drivers(
      driverID: json['driverID'],
      userID: json['userID']['userID'] != null ? !(json['userID']['userID'] is String) && json['userID']['userID'] != null : json['userID'],
      status: json['status'],
      lastLat: json['lastLat'] ? json['lastLat'] as double : 0,
      lastLng: json['lastLng'] ? json['lastLng'] as double : 0,
      createdAt: json['createdAt'],
      rating: json['rating'] ? json['rating'] as double : 0,
      rateTimes: json['rateTimes'] ? json['rateTimes'] as int : 0,
      userData: Users.fromJson(json['userID'])
    );

    return driver;
  }
}