import 'package:booking_grab_apis/models/position.dart';
import 'package:booking_grab_apis/models/users.dart';

class VehicleBookings {
  String vehicleBookingID;
  String driverID;
  String customerID;
  MyPosition startingPoint;
  MyPosition destination;
  String startAddress;
  String destinationAddress;
  double distances;
  double fee;
  double price;
  DateTime startedAt;
  DateTime finishedAt;
  String status;
  DateTime createdAt;
  DateTime updatedAt;
  Users customerData;

  VehicleBookings({
    this.vehicleBookingID,
    this.driverID,
    this.customerID,
    this.startingPoint,
    this.destination,
    this.startAddress,
    this.destinationAddress,
    this.distances,
    this.fee,
    this.price,
    this.startedAt,
    this.finishedAt,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.customerData
  });

  factory VehicleBookings.fromJson(Map<String, dynamic> json) {
    VehicleBookings vehicleBooking = new VehicleBookings(
      vehicleBookingID: json['vehicleBookingID'],
      driverID: (json['driverID'] != null && !(json['driverID'] is String) && json['driverID']['driverID'] != null ) ? json['driverID']['driverID'] : json['driverID'],
      customerID:(json['customerID'] != null && !(json['customerID'] is String) && json['customerID']['userID'] != null) ? json['customerID']['userID'] : json['customerID'],
      startingPoint: MyPosition.fromJson(json['startingPoint']),
      destination: MyPosition.fromJson(json['destination']),
      startAddress: json['startAddress'],
      destinationAddress: json['destinationAddress'],
      distances: double.parse(json['distances'].toString()),
      fee: double.parse(json['fee'].toString()),
      price: double.parse(json['price'].toString()),
      status: json['status'],
      customerData: json['customerID'] != null && !(json['customerID'] is String) && json['customerID']['userID'] != null ? Users.fromJson(json['customerID']) : null
    );

    return vehicleBooking;
  }
}