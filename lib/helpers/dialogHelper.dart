import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/main.dart';
import 'package:flutter/material.dart';
class MyActions {
  Function handle;
  String name;
  Color color;

  MyActions({this.handle, this.name, this.color});
}
class DialogHelper {
  showNotification(
    String content,
    List<MyActions> actions,
  ) {
    showModalBottomSheet(
      context: navigatorKey.currentContext,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(20),
          topRight: const Radius.circular(20)
        ),
      ),
      barrierColor: Colors.black.withOpacity(0.3),
      builder: (BuildContext bc){
        return Container(
          height: 220,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
          decoration: BoxDecoration(
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20)
            ),
            boxShadow: [
              BoxShadow(
                blurRadius: 20,
                color: Colors.grey,
                spreadRadius: 1
              )
            ],
            color: myColors.whiteBackground
          ),
          child: Stack(
            fit: StackFit.loose,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 120,
                  child: Center(
                    child: Text(
                      content,
                      style: TextStyle(
                        color: myColors.mainColor,
                        fontSize: 18,
                      ),
                    ),
                  ),
                )
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  alignment: Alignment.center,
                  height: 100,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: buildAction(actions)
                  )
                )
              )
            ],
          )
        );
      }
    );
  }

  List<Widget> buildAction(List<MyActions> actions) {
    List<Widget> listWidget = new List<Widget>();
    actions.forEach((element) { 
      listWidget.add(
        new Container(
          width: 200,
          padding: EdgeInsets.all(10),
          height: 80,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text(
                element.name,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                ),
              ),
            ),
            color: element.color,
            onPressed: element.handle
          ),
        )
      );
    });

    return listWidget;
  }
}