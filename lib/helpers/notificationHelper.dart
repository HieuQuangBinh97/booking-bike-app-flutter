import 'package:booking_grab_apis/constants/variable.dart';
import 'package:booking_grab_apis/models/notifications.dart';

class NotificationHelper {
  void addNotification(data) {
    try {
      Notifications newNotification = Notifications.fromJson(data['notification']);

      if (MyGlobalVariable.notifications == null) {
        MyGlobalVariable.notifications = new List<Notifications>();
        MyGlobalVariable.notifications.add(newNotification);

        return;
      }

      MyGlobalVariable.notifications.add(newNotification);
      MyGlobalVariable.lastNotificationID = newNotification.notificationID;
      return;
    } catch(error) {
      print(error);
    }
  }
}