import 'package:booking_grab_apis/constants/colors.dart';
import 'package:booking_grab_apis/models/notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastHelper {
  void showTopShortToastError(String message) {
    Fluttertoast.showToast(
        msg: message,
        textColor: myColors.whiteText,
        backgroundColor: myColors.accentColor,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  void showTopShortToastSuccessfuly(String message) {
    Fluttertoast.showToast(
        msg: message,
        textColor: myColors.whiteText,
        backgroundColor: myColors.mainColor,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20,
    );
  }

  // void showTopToastNotification(Notifications notifications) {
  //   Fluttertoast.showToast(
  //       msg: message,
  //       textColor: myColors.whiteText,
  //       backgroundColor: myColors.mainColor,
  //       toastLength: Toast.LENGTH_LONG,
  //       gravity: ToastGravity.TOP,
  //       timeInSecForIosWeb: 2,
  //       fontSize:20,

  //   );
  // }
}